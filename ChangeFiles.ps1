#Add-Type -Path .\MathNet.Numerics.4.7.0\lib\net40\MathNet.Numerics.dll
#[psobject].Assembly.GetType("System.Management.Automation.TypeAccelerators")::Add('M','MathNet.Numerics.LinearAlgebra.Matrix[Double]')
#[psobject].Assembly.GetType("System.Management.Automation.TypeAccelerators")::Add('V','MathNet.Numerics.LinearAlgebra.Double.DenseVector')

$paramFile = $args[0]
$outputFile = $args[1]
function Convert-XmlElementToString
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        $xml
    )

    $sw = [System.IO.StringWriter]::new()
    $xmlSettings = [System.Xml.XmlWriterSettings]::new()
    $xmlSettings.ConformanceLevel = [System.Xml.ConformanceLevel]::Fragment
    $xmlSettings.Indent = $true
    $xw = [System.Xml.XmlWriter]::Create($sw, $xmlSettings)
    $xml.WriteTo($xw)
    $xw.Close()
    return $sw.ToString()
}

if(Test-Path $paramFile)
{
    $hipsName = ""
    $pathString = Split-Path -Path $paramFile;
    $workDir = (Get-ChildItem -Path $pathString);
    $mainFile = Split-Path $paramFile -leaf;

    $outputWorkDir = Split-Path -Path $outputFile;
    
    $xml = [xml](Get-Content (Join-Path $pathString $mainFile) -Raw);
    $ns = New-Object System.Xml.XmlNamespaceManager($xml.NameTable)
    $ns.AddNamespace("ns", $xml.DocumentElement.NamespaceURI)

    foreach($file in $workDir)
    {
        if($file.Extension.ToUpper() -eq ".DAE")
        {
            if( $file.name -ne $mainFile)
            {
                $newxml = [xml](Get-Content (Join-Path $pathString $file.name ) -Raw);
                $ns1 = New-Object System.Xml.XmlNamespaceManager($newxml.NameTable)
                $ns1.AddNamespace("ns", $newxml.DocumentElement.NamespaceURI)
                $animationNode = $newxml.SelectSingleNode("//ns:library_animations", $ns1)
                
                $hips_animations = $animationNode.SelectSingleNode("ns:animation[contains(@id,'Hips')]", $ns1);
                $hipsName = $hips_animations.GetAttribute("name")
                $root_doc_animation = $hips_animations.Clone();

                $transforms_node = $hips_animations.SelectSingleNode(".//ns:float_array[contains(@id,'output')]", $ns1);
                $transforms_node_root = $root_doc_animation.SelectSingleNode(".//ns:float_array[contains(@id,'output')]", $ns1);
                
                $num_keys = [int]$transforms_node.GetAttribute("count") / 16;
                $keys_hips = $transforms_node.innerText.Trim() -split '\s+|\r\n';
                $keys_root = $transforms_node_root.innerText.Trim() -split '\s+|\r\n';

                for($keycount =0; $keycount -lt $num_keys; $keycount++)
                {
                    if($file.name.Contains("Idle") -or $file.name.Contains("Shot"))
                    {
                        #Set identity matrix for all, this will prevent moving when static (idle)
                        $keys_root[($keycount*16) + 0] = "1.000000";
                        $keys_root[($keycount*16) + 1] = "0.000000";
                        $keys_root[($keycount*16) + 2] = "0.000000";
                        $keys_root[($keycount*16) + 3] = "0.000000";

                        $keys_root[($keycount*16) + 4] = "0.000000";
                        $keys_root[($keycount*16) + 5] = "1.000000";
                        $keys_root[($keycount*16) + 6] = "0.000000";
                        $keys_root[($keycount*16) + 7] = "0.000000";

                        $keys_root[($keycount*16) + 8] = "0.000000";
                        $keys_root[($keycount*16) + 9] = "0.000000";
                        $keys_root[($keycount*16) + 10] = "1.000000";
                        $keys_root[($keycount*16) + 11] = "0.000000";

                        $keys_root[($keycount*16) + 12] = "0.000000";
                        $keys_root[($keycount*16) + 13] = "0.000000";
                        $keys_root[($keycount*16) + 14] = "0.000000";
                        $keys_root[($keycount*16) + 15] = "1.000000";
                    }
                    else
                    {
                        #Set identity matrix, except for XZ translation
                        $keys_root[($keycount*16) + 0] = "1.000000";
                        $keys_root[($keycount*16) + 1] = "0.000000";
                        $keys_root[($keycount*16) + 2] = "0.000000";
                        #$keys_root[($keycount*16) + 3] = "0.000000";

                        $keys_root[($keycount*16) + 4] = "0.000000";
                        $keys_root[($keycount*16) + 5] = "1.000000";
                        $keys_root[($keycount*16) + 6] = "0.000000";
                        $keys_root[($keycount*16) + 7] = "0.000000";

                        $keys_root[($keycount*16) + 8] = "0.000000";
                        $keys_root[($keycount*16) + 9] = "0.000000";
                        $keys_root[($keycount*16) + 10] = "1.000000";
                        #$keys_root[($keycount*16) + 11] = "0.000000";

                        $keys_root[($keycount*16) + 12] = "0.000000";
                        $keys_root[($keycount*16) + 13] = "0.000000";
                        $keys_root[($keycount*16) + 14] = "0.000000";
                        $keys_root[($keycount*16) + 15] = "1.000000";

                        #Clean XZ translation
                        $keys_hips[($keycount*16) + 3] = "0.000000";
                        $keys_hips[($keycount*16) + 11] = "0.000000";
                    }
                }

                $transforms_node.InnerText = "$keys_hips";
                $transforms_node_root.InnerText = "$keys_root";

                $parsed_node = Convert-XmlElementToString $root_doc_animation;
                $parsed_node = $parsed_node -replace $hipsName, "root_doc"

                $new_node = [xml]$parsed_node;
                $animationNode.InsertBefore($newxml.ImportNode($new_node.animation,$true) , $hips_animations);
                #TODO: SAVE FILE
                $animFileOutput = (Join-Path $outputWorkDir $file.name ) 
                $newxml.Save($animFileOutput)
            }
        }
    }

    #Validate the morphs
    $controllersNode = $xml.SelectSingleNode("//ns:library_controllers", $ns)
    foreach($controller in $controllersNode.ChildNodes)
    {
        $skinNode = $controller.SelectSingleNode("ns:skin",$ns);
        if($skinNode)
        {
            $morphName =  $skinNode.GetAttribute("source").Substring(1) + "-morph";
            $morphController = $controllersNode.SelectSingleNode("ns:controller[@id='" + $morphName + "']",$ns); 
            if($morphController)
            {
                $morphController.FirstChild.SetAttribute("method", "NORMALIZED");
                $skinNode.SetAttribute("source", "#" + $morphName);
            }
        }
    }
    #Add new root node
    $visualScene = $xml.SelectSingleNode("//ns:visual_scene", $ns)
    $hips = $visualScene.SelectSingleNode("ns:node[@id='" + $hipsName + "']",$ns);
    $hips_matrix = $hips.SelectSingleNode("ns:matrix",$ns);

    $matrix_values = $hips_matrix.innerText.Trim() -split '\s+|\r\n';
    
    $root_doc = $xml.CreateElement("node");
    $root_doc.SetAttribute("id", "root_doc");
    $root_doc.SetAttribute("name", "root_doc");
    $root_doc.SetAttribute("sid", "root_doc");
    $root_doc.SetAttribute("type", "JOINT");
    
    $root_matrix = $xml.CreateElement("matrix");
    $root_matrix.SetAttribute("sid", "matrix");
    $Xpos = $matrix_values[3];
    $Zpos = $matrix_values[11];
    if($file.name.Contains("Idle") -or $file.name.Contains("Shot"))
    {
        $root_matrix.innerText = "1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000";            
    }
    else 
    {
        $root_matrix.innerText = "1.000000 0.000000 0.000000 " + $Xpos + " 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 " + $Zpos + " 0.000000 0.000000 0.000000 1.000000";    
    }
    
    $root_doc.AppendChild($root_matrix);

    if(!$file.name.Contains("Idle") -and !$file.name.Contains("Shot"))
    {
        $matrix_values[3] = "0.000000";
        $matrix_values[11] = "0.000000";
    }
    $hips_matrix.InnerText = "$matrix_values";

    $visualScene.RemoveChild($hips);
    $root_doc.AppendChild($hips);
    $visualScene.AppendChild($root_doc);
    
    $skeleNodes = $visualScene.SelectNodes("//ns:skeleton", $ns);
    foreach($skeleNode in $skeleNodes)
    {
        $skeleNode.InnerText = "#root_doc";
    }

    #Modify binding matrices for skin controllers
    $skins =$xml.SelectNodes("//ns:skin", $ns);
    $countering = 0;
    foreach($skin in $skins)
    {
        $joints = $skin.SelectSingleNode("ns:source[contains(@id,'Joints')]", $ns);
        $matrices = $skin.SelectSingleNode("ns:source[contains(@id,'Matrices')]", $ns);
        
        $name_array = $joints.SelectSingleNode("ns:Name_array",$ns);
        $matrices_array = $matrices.SelectSingleNode("ns:float_array",$ns);

        $name_array.SetAttribute("count", [int]$name_array.GetAttribute("count") + 1);
        $name_array.innerText += " root_doc";
        $accessor = $joints.SelectSingleNode(".//ns:accessor", $ns1);
        $accessor.SetAttribute("count", [int]$accessor.GetAttribute("count") + 1);

        $matrices_array.SetAttribute("count", [int]$matrices_array.GetAttribute("count") + 16);
        $matrices_array.innerText += " 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000";
        $accessor = $matrices.SelectSingleNode(".//ns:accessor", $ns1);
        $accessor.SetAttribute("count", [int]$accessor.GetAttribute("count") + 1);

        $countering++;
    }
    $xml.Save($outputFile)
    Write-Output "SKINS: ", $countering;
    Write-Output "Done";
}
else 
{
    Write-Output "Invalid file name";
}