$ErrorActionPreference = "Stop"
$paramFile = $args[0]
if(Test-Path $paramFile)
{
    [xml]$ogreFile = New-Object System.Xml.XmlDocument
    $pathString = Split-Path -Path $paramFile;
    #$workDir = (Get-ChildItem -Path $pathString);
    $mainFile = Split-Path $paramFile -leaf;
    $mixamoFile = [xml](Get-Content (Join-Path $pathString $mainFile) -Raw);
    $ns = New-Object System.Xml.XmlNamespaceManager($mixamoFile.NameTable)
    $ns.AddNamespace("ns", $mixamoFile.DocumentElement.NamespaceURI)

    $mainMesh = $ogreFile.CreateElement("mesh");
    $subMeshes = $ogreFile.CreateElement("submeshes");

    #$animationNode = $mixamoFile.SelectSingleNode("//ns:library_animations", $ns)
    #Get the submeshes, they are in controller and have a skin element
    $controllersNode = $mixamoFile.SelectSingleNode("//ns:library_controllers", $ns)
    $meshIndex = 0;
    $meshesNames = $ogreFile.CreateElement("submeshnames");
    foreach($controller in $controllersNode.ChildNodes)
    {
        if($controller.FirstChild.name -eq "skin" )
        {
            $source = $controller.FirstChild.GetAttribute("source").Substring(1);

            $newMeshName = $ogreFile.CreateElement("submeshname");
            $newMeshName.SetAttribute("index", $meshIndex);
            $newMeshName.SetAttribute("name", $source);
            $meshesNames.AppendChild($newMeshName);
            $meshIndex++;

            $geometry = $mixamoFile.SelectSingleNode("//ns:geometry[@id='" + $source + "']", $ns);

            $polylist = $geometry.SelectSingleNode("ns:mesh/ns:polylist",$ns); 

            $submesh = $ogreFile.CreateElement("submesh");

            [int]$numfaces = [int]$polylist.GetAttribute("count");

            $faces = $ogreFile.CreateElement("faces");

            $faces.SetAttribute("count", $numfaces);

            $polygons = $polylist.SelectSingleNode("ns:p",$ns);
            
            $polyArray = $polygons.innerText.Trim() -split '\s+|\r\n'; 
            #Always triangles, order is Vertex, Normal, UV
            for($p = 0; $p -lt $numfaces; $p++)
            {
                $newFace = $ogreFile.CreateElement("face");
                $offset = 3 * 3 * $p;
                $newFace.SetAttribute("v3", $polyArray[$offset+6]);
                $newFace.SetAttribute("v2", $polyArray[$offset+3]);
                $newFace.SetAttribute("v1", $polyArray[$offset+0]);
                $faces.AppendChild($newFace);
            }
            
            $submesh.AppendChild($faces)

            #Add geometry

            $positionsArray = $geometry.SelectSingleNode("ns:mesh/ns:source[@name='position']/ns:float_array", $ns).innerText.Trim() -split '\s+|\r\n';
            $normalsArray = $geometry.SelectSingleNode("ns:mesh/ns:source[@name='normal']/ns:float_array", $ns).innerText.Trim() -split '\s+|\r\n';
            $uvsArray = $geometry.SelectSingleNode("ns:mesh/ns:source[@name='map1']/ns:float_array", $ns).innerText.Trim() -split '\s+|\r\n';

            $newGeometry = $ogreFile.CreateElement("geometry");

            [int]$numVertex = [int]($positionsArray.Length / 3);
            $newGeometry.SetAttribute("vertexcount", $numVertex);

            $posnormsBuffer = $ogreFile.CreateElement("vertexbuffer");

            $posnormsBuffer.SetAttribute("normals", "true");
            $posnormsBuffer.SetAttribute("positions", "true");
            for($v = 0; $v -lt $numVertex; $v++)
            {
                $newVertex = $ogreFile.CreateElement("vertex");
                $newPos = $ogreFile.CreateElement("position");
                $newNormal = $ogreFile.CreateElement("normal");
                $newPos.SetAttribute("z",$positionsArray[($v*3)+2]);
                $newPos.SetAttribute("y",$positionsArray[($v*3)+1]);
                $newPos.SetAttribute("x",$positionsArray[($v*3)+0]);
                $newNormal.SetAttribute("z",$normalsArray[($v*3)+2]);
                $newNormal.SetAttribute("y",$normalsArray[($v*3)+1]);
                $newNormal.SetAttribute("x",$normalsArray[($v*3)+0]);
                $newVertex.AppendChild($newNormal);
                $newVertex.AppendChild($newPos);
                $posnormsBuffer.AppendChild($newVertex);
            }
            $newGeometry.AppendChild($posnormsBuffer);


            $uvsBuffer = $ogreFile.CreateElement("vertexbuffer");

            $uvsBuffer.SetAttribute("texture_coords", "1");
            $uvsBuffer.SetAttribute("texture_coord_dimensions_0", "float2");
            [int]$numUVs = [int]($uvsArray.Length / 2)
            for($uv = 0; $uv -lt $numUVs; $uv++)
            {
                $newVertex = $ogreFile.CreateElement("vertex");
                $newUV = $ogreFile.CreateElement("texcoord");
                $newUV.SetAttribute("v",$normalsArray[($uv*2)+1]);
                $newUV.SetAttribute("u",$normalsArray[($uv*2)+0]);
                $newVertex.AppendChild($newUV);
                $uvsBuffer.AppendChild($newVertex);
            }
            $newGeometry.AppendChild($uvsBuffer);

            $submesh.AppendChild($newGeometry);

            $submesh.SetAttribute("material", "Materials/" + $polylist.GetAttribute("material") );
            $submesh.SetAttribute("usesharedvertices", "false"); 
            $submesh.SetAttribute("use32bitindexes", "false");
            $submesh.SetAttribute("operationtype", "triangle_list");

            
            $subMeshes.AppendChild($submesh)
        }
        #TODO: Add skelton and skeleton bindings
    }

    $poses = $ogreFile.CreateElement("poses");
    $animations = $ogreFile.CreateElement("animations");
    #Save the poses index
    $globalPoseIndex = 0;
    foreach($controller in $controllersNode.ChildNodes)
    {
        if($controller.FirstChild.name -eq "morph" )
        {
            $morph = $controller.FirstChild;
            $source = $controller.FirstChild.GetAttribute("source").Substring(1);

            $meshname = $meshesNames.SelectSingleNode("submeshname[@name='" + $source + "']");
            [int]$index = [int]($meshname.GetAttribute("index"));
            $posesListArray = $morph.SelectSingleNode("ns:source/ns:IDREF_array", $ns).innerText.Trim() -split '\s+|\r\n';

            $geometryBase = $mixamoFile.SelectSingleNode("//ns:geometry[@id='" + $source + "']", $ns);
            $positionsArrayBase = $geometryBase.SelectSingleNode("ns:mesh/ns:source[@name='position']/ns:float_array", $ns).innerText.Trim() -split '\s+|\r\n';
    
            foreach($poseName in $posesListArray)
            {
                $geometry = $mixamoFile.SelectSingleNode("//ns:geometry[@id='" + $poseName + "']", $ns);
                $positionsArray = $geometry.SelectSingleNode("ns:mesh/ns:source[@id='" + $poseName + "-lib-Position']/ns:float_array", $ns).innerText.Trim() -split '\s+|\r\n';
              
                $pose = $ogreFile.CreateElement("pose");
                $pose.SetAttribute("name", $poseName);
                $pose.SetAttribute("index", $index);
                $pose.SetAttribute("target", "submesh");
                [int]$numVertex = [int]($positionsArray.Length / 3);
                for($v = 0; $v -lt $numVertex; $v++)
                {
                    $offsetz = $positionsArrayBase[($v*3)+2] - $positionsArray[($v*3)+2];
                    $offsety = $positionsArrayBase[($v*3)+1] - $positionsArray[($v*3)+1];
                    $offsetx = $positionsArrayBase[($v*3)+0] - $positionsArray[($v*3)+0];
                    if($offsetz -ne 0 -and $offsety -ne 0 -and $offsetx -ne 0)
                    {
                        $poseOffset = $ogreFile.CreateElement("poseoffset");
                        $poseOffset.SetAttribute("z",$offsetz);
                        $poseOffset.SetAttribute("y",$offsety);
                        $poseOffset.SetAttribute("x",$offsetx);
                        $poseOffset.SetAttribute("index", $v);
                        $pose.AppendChild($poseOffset);
                    }
                }
                #Create Animations
                $animationName = $geometry.GetAttribute("name");
                $animation = $ogreFile.CreateElement("animation");
                $animation.SetAttribute("length", "1");
                $animation.SetAttribute("name", $animationName);

                $tracks = $ogreFile.CreateElement("tracks");
                $track = $ogreFile.CreateElement("track");

                $track.SetAttribute("type","pose");
                $track.SetAttribute("index",$index); #Submesh index
                $track.SetAttribute("target","submesh");

                $keyframes = $ogreFile.CreateElement("keyframes");
                $key1 = $ogreFile.CreateElement("keyframe");
                $key1.SetAttribute("time", "0");
                $keyframes.AppendChild($key1);
                $key2 = $ogreFile.CreateElement("keyframe");
                $key2.SetAttribute("time", "1");
                $poseref= $ogreFile.CreateElement("poseref");
                $poseref.SetAttribute("influence", "1.0");
                $poseref.SetAttribute("index", $globalPoseIndex);
                $key2.AppendChild($poseref);
                $keyframes.AppendChild($key2);
                $track.AppendChild($keyframes);
                $tracks.AppendChild($track);
                $animation.AppendChild($tracks);
                $animations.AppendChild($animation);
                $poses.AppendChild($pose);
                $globalPoseIndex ++;
            }
        }
    }
    

    $mainMesh.AppendChild($subMeshes);
    $mainMesh.AppendChild($meshesNames);
    $mainMesh.AppendChild($poses);
    $mainMesh.AppendChild($animations);

    $ogreFile.AppendChild($mainMesh);
    $ogreFile.Save((Join-Path $pathString "mixamo.mesh.xml"))    
    Write-Output "Done";
}
else 
{
    Write-Output "Invalid file name";
}
