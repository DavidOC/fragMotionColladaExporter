------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- COLLADA Export Plugin version 5.0
-- This script exports the scene information to COLLADA format.
-- Copyright (C) David Ochoa
-- DISCLAIMER: This script is provided "AS IS" without warranty of any kind, you are free to use/modify it.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ExportMesh(exportingMesh, uniqueMesh)
	local verts, vert_count, faces, face_count = exportingMesh:LockMesh();
	local strVert   = "";
	local strUV0    = "";
	local strColor  = "";
	local strNormal = "";
	local strMeshSufix = string.gsub(exportingMesh.Name," ","_")..uniqueMesh;

	print("Exporting "..face_count.." tris");
	file:write("    <geometry id='MESH_LIB_"..strMeshSufix.."' name='MESH_LIB_"..strMeshSufix.."'>\n");
	file:write("      <mesh>\n");
	--Write the information, later the polygons will access this information by index
	file:write("        <source id='MESH_LIB_POS_"..strMeshSufix.."'>\n");
	file:write("          <float_array id='MESH_LIB_POS_ARRAY_"..strMeshSufix.."' count='"..(vert_count * 3).."'>");
    print("Exporting Vertices");
	--Get vertex position, it comes from the verts array
	for i = 0, vert_count-1, 1 do
	   file:write("\n"..string.format("%f",verts[i].x).." "..string.format("%f",verts[i].y).." "..string.format("%f",verts[i].z).." ");
	end
    file:write("          </float_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#MESH_LIB_POS_ARRAY_"..strMeshSufix.."' count='"..vert_count.."' stride='3'>\n");
	file:write("              <param name='X' type='float'/>\n");
	file:write("              <param name='Y' type='float'/>\n");
	file:write("              <param name='Z' type='float'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");
	strVert = nil;
	
	file:write("        <source id='MESH_LIB_UV0_"..strMeshSufix.."'>\n");
	file:write("          <float_array id='MESH_LIB_UV0_ARRAY_"..strMeshSufix.."' count='"..(face_count * 2 * 3).."'>\n")
    print("Exporting UV");
	--Get UV information, it comes in the faces array
	for i = 0, face_count-1, 1 do
	   file:write(formatFloat(faces[i].uv[0].x).." "..formatFloat(1 - faces[i].uv[0].y).." ");
	   file:write(formatFloat(faces[i].uv[1].x).." "..formatFloat(1 - faces[i].uv[1].y).." ");
	   file:write(formatFloat(faces[i].uv[2].x).." "..formatFloat(1 - faces[i].uv[2].y).." \n");
	end 
    
    
	file:write("          </float_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#MESH_LIB_UV0_ARRAY_"..strMeshSufix.."' count='"..(face_count * 3).."' stride='2'>\n");
	file:write("              <param name='S' type='float'/>\n");
	file:write("              <param name='T' type='float'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");
	strUV0 = nil;
	
	file:write("        <source id='MESH_LIB_NOR_"..strMeshSufix.."'>\n");
	file:write("          <float_array id='MESH_LIB_NOR_ARRAY_"..strMeshSufix.."' count='"..(face_count * 3 * 3).."'>\n");
    print("Exporting Normals");
	--Get normal information, it comes in the faces array
	for i = 0, face_count-1, 1 do
	   file:write(formatFloat(faces[i].normals[0].x).." "..formatFloat(faces[i].normals[0].y).." "..formatFloat(faces[i].normals[0].z).." ");
	   file:write(formatFloat(faces[i].normals[1].x).." "..formatFloat(faces[i].normals[1].y).." "..formatFloat(faces[i].normals[1].z).." ");
	   file:write(formatFloat(faces[i].normals[2].x).." "..formatFloat(faces[i].normals[2].y).." "..formatFloat(faces[i].normals[2].z).." \n");
	end 
    file:write("          </float_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#MESH_LIB_NOR_ARRAY_"..strMeshSufix.."' count='"..(face_count *3).."' stride='3'>\n");
	file:write("              <param name='X' type='float'/>\n");
	file:write("              <param name='Y' type='float'/>\n");
	file:write("              <param name='Z' type='float'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");
	strNormal = nil;

	file:write("        <vertices id='MESH_LIB_VERTEX_"..strMeshSufix.."'>\n");
	file:write("          <input semantic='POSITION' source='#MESH_LIB_POS_"..strMeshSufix.."'/>\n");
	file:write("        </vertices>\n");
    print("Exporting Polygons");
	--The polygons are written by material, so we will iterate through each material
	currMaterial = allMaterials:GetFirstChild();
	while currMaterial:isSet() do
		local trisPerSurface = 0;
		for i = 0, face_count-1, 1 do
			if faces[i].matID == currMaterial:GetID() then
				trisPerSurface = trisPerSurface + 1;
			end
		end
		--We need to be sure there are polygons assigned to this material
		if trisPerSurface > 0 then
			--We need to indicate from where we are taking the information (source)
			file:write("        <triangles material='"..string.gsub(currMaterial:GetName()," ","_").."' count='"..trisPerSurface.."'>\n");
			file:write("        	<input semantic='VERTEX'   offset='0' source='#MESH_LIB_VERTEX_"..strMeshSufix.."'/>\n");
			file:write("        	<input semantic='NORMAL'   offset='1' source='#MESH_LIB_NOR_"..strMeshSufix.."'/>\n");
			file:write("        	<input semantic='TEXCOORD' offset='2' set='0' source='#MESH_LIB_UV0_"..strMeshSufix.."'/>\n");
			file:write("        	<p>\n");
			--Iterate the triangles
			for i = 0, face_count-1, 1 do
				--Write the polygon is it's assigned to this material
				if faces[i].matID == currMaterial:GetID() then
					local vT0 = faces[i].index[0];	--Vertex 1 index
					local vT1 = faces[i].index[1];	--Vertex 2 index
					local vT2 = faces[i].index[2];	--Vertex 3 index
					local oT0 = i * 3;				--We just wrote the information without checking for duplicates, so the index is the same as the face index * 3
					local oT1 = (i * 3) + 1;
					local oT2 = (i * 3) + 2;
					file:write("          "..vT0.." "..oT0.." "..oT0.." "..vT2.." "..oT2.." "..oT2.." "..vT1.." "..oT1.." "..oT1.."\n");
					file:flush();
				end
			end
			file:write("        	</p>\n");
			file:write("        </triangles>\n");
		end
		currMaterial = currMaterial:GetNextSibling();
	end
	exportingMesh:UnlockMesh();	
	file:write("      </mesh>\n");
	file:write("    </geometry>\n");
	file:flush();
end

function process_keys(meshKeyList, meshKey)

   globalKeyCounter = globalKeyCounter + 1;

	print("Processing morph key "..globalKeyCounter);

	local theRealMeshKey = IMeshKey(meshKey);
	local exportingMesh = editChar:GetDefaultMesh();
	
	local verts, vert_count, faces, face_count = exportingMesh:LockMesh();
	local infoFaces, infoNumFaces, infoNormals, infoNumNormals = exportingMesh:GetNormalInfo();	

	local vertsMorp, vert_countMorp, normals, numnormals = theRealMeshKey:LockValues();
	theRealMeshKey:UnlockValues();
	exportingMesh:UnlockMesh();
	
		
	print(infoNumFaces * 3);

	local pvertIndex = WORD_Array();
	
	pvertIndex:resize(infoNumFaces * 3);
	
	
	for jj=0, infoNumFaces - 1, 1 do
		pvertIndex[faces[jj].index[0]] = infoFaces[jj].index[0];
		pvertIndex[faces[jj].index[1]] = infoFaces[jj].index[1];
		pvertIndex[faces[jj].index[2]] = infoFaces[jj].index[2];
	end
	


	--[[
	print("Got indices");
	
	local vert_countMorp = vert_count;
	local vertsMorp = vertex3d_Array();
	vertsMorp:resize(vert_countMorp);

	theRealMeshKey:GetVertices(vertsMorp, pvertIndex, vert_countMorp);

	print("Got vertices: "..vert_countMorp);
	
	local numnormals = theRealMeshKey:GetNumNormals();
	print(numnormals);
	local normals = vertex3d_Array();
	normals:resize(numnormals);
	print("resized");
	theRealMeshKey:GetNormals(normals,pvertIndex,numnormals);
	print(numnormals);
	
	print(vert_countMorp.." "..numnormals.." "..normals:size());
	
	print("Got normals");
	--]]
	
	
	
	
	local strVert   = "";
	local strUV0    = "";
	local strColor  = "";
	local strNormal = "";
	local strMeshSufix = "MORPH_"..globalKeyCounter;

	print("Exporting "..face_count.." tris for MORPH "..globalKeyCounter);
		
	file:write("    <geometry id='MESH_LIB_"..strMeshSufix.."' name='MESH_LIB_"..strMeshSufix.."'>\n");
	file:write("      <mesh>\n");
	--Write the information, later the polygons will access this information by index
	--Get the positions from the MORPH
	file:write("        <source id='MESH_LIB_POS_"..strMeshSufix.."'>\n");
	file:write("          <float_array id='MESH_LIB_POS_ARRAY_"..strMeshSufix.."' count='"..(vert_countMorp * 3).."'>");
    print("Exporting Vertices");
	--Get vertex position, it comes from the verts array
	for i = 0, vert_countMorp - 1, 1 do
	   file:write("\n"..string.format("%f",vertsMorp[i].x).." "..string.format("%f",vertsMorp[i].y).." "..string.format("%f",vertsMorp[i].z).." ");
	end
    file:write("          </float_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#MESH_LIB_POS_ARRAY_"..strMeshSufix.."' count='"..vert_countMorp.."' stride='3'>\n");
	file:write("              <param name='X' type='float'/>\n");
	file:write("              <param name='Y' type='float'/>\n");
	file:write("              <param name='Z' type='float'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");
	strVert = nil;
	
	file:write("        <source id='MESH_LIB_UV0_"..strMeshSufix.."'>\n");
	file:write("          <float_array id='MESH_LIB_UV0_ARRAY_"..strMeshSufix.."' count='"..(face_count * 2 * 3).."'>\n")
    print("Exporting UV");
	--Get UV information, it comes in the faces array
	for i = 0, face_count-1, 1 do
	   file:write(formatFloat(faces[i].uv[0].x).." "..formatFloat(1 - faces[i].uv[0].y).." ");
	   file:write(formatFloat(faces[i].uv[1].x).." "..formatFloat(1 - faces[i].uv[1].y).." ");
	   file:write(formatFloat(faces[i].uv[2].x).." "..formatFloat(1 - faces[i].uv[2].y).." \n");
	end 
    
    
	file:write("          </float_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#MESH_LIB_UV0_ARRAY_"..strMeshSufix.."' count='"..(face_count * 3).."' stride='2'>\n");
	file:write("              <param name='S' type='float'/>\n");
	file:write("              <param name='T' type='float'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");
	strUV0 = nil;
	
	
	-- GET THE NORMALS FROM THE MORPH	
	print("Exporting Normals");

	file:write("        <source id='MESH_LIB_NOR_"..strMeshSufix.."'>\n");
	file:write("          <float_array id='MESH_LIB_NOR_ARRAY_"..strMeshSufix.."' count='"..(face_count * 3 * 3).."'>\n");
	
		
	for i = 0, face_count-1, 1 do
		file:write(formatFloat(normals[infoFaces[i].index[0]].x).." "..formatFloat(normals[infoFaces[i].index[0]].y).." "..formatFloat(normals[infoFaces[i].index[0]].z).." ");
		file:write(formatFloat(normals[infoFaces[i].index[1]].x).." "..formatFloat(normals[infoFaces[i].index[1]].y).." "..formatFloat(normals[infoFaces[i].index[1]].z).." ");
		file:write(formatFloat(normals[infoFaces[i].index[2]].x).." "..formatFloat(normals[infoFaces[i].index[2]].y).." "..formatFloat(normals[infoFaces[i].index[2]].z).."\n");
	end

	--theRealMeshKey:foreach("normal", print_normals);
	
	file:write("          </float_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#MESH_LIB_NOR_ARRAY_"..strMeshSufix.."' count='"..(face_count * 3).."' stride='3'>\n");
	file:write("              <param name='X' type='float'/>\n");
	file:write("              <param name='Y' type='float'/>\n");
	file:write("              <param name='Z' type='float'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");

	
	

	file:write("        <vertices id='MESH_LIB_VERTEX_"..strMeshSufix.."'>\n");
	file:write("          <input semantic='POSITION' source='#MESH_LIB_POS_"..strMeshSufix.."'/>\n");
	file:write("        </vertices>\n");
	
   print("Exporting Polygons");
	--The polygons are written by material, so we will iterate through each material
	currMaterial = allMaterials:GetFirstChild();
	while currMaterial:isSet() do
		local trisPerSurface = 0;
		for i = 0, face_count-1, 1 do
			if faces[i].matID == currMaterial:GetID() then
				trisPerSurface = trisPerSurface + 1;
			end
		end
		--We need to be sure there are polygons assigned to this material
		if trisPerSurface > 0 then
			--We need to indicate from where we are taking the information (source)
			file:write("        <triangles material='"..string.gsub(currMaterial:GetName()," ","_").."' count='"..trisPerSurface.."'>\n");
			file:write("        	<input semantic='VERTEX'   offset='0' source='#MESH_LIB_VERTEX_"..strMeshSufix.."'/>\n");
			file:write("        	<input semantic='NORMAL'   offset='1' source='#MESH_LIB_NOR_"..strMeshSufix.."'/>\n");
			file:write("        	<input semantic='TEXCOORD' offset='2' set='0' source='#MESH_LIB_UV0_"..strMeshSufix.."'/>\n");
			file:write("        	<p>\n");
			--Iterate the triangles
			for i = 0, face_count-1, 1 do
				--Write the polygon is it's assigned to this material
				--ASUME THE INDEX IS THE SAME FOR BOTH ARRAYS ...
				--HOPE IT'S ALLRIGHT
				if faces[i].matID == currMaterial:GetID() then
					local vT0 = faces[i].index[0];	--Vertex 1 index
					local vT1 = faces[i].index[1];	--Vertex 2 index
					local vT2 = faces[i].index[2];	--Vertex 3 index
					local oT0 = i * 3;				--We just wrote the information without checking for duplicates, so the index is the same as the face index * 3
					local oT1 = (i * 3) + 1;
					local oT2 = (i * 3) + 2;
					
					file:write("          "..vT0.." "..vT0.." "..oT0.." "..vT2.." "..vT2.." "..oT2.." "..vT1.." "..vT1.." "..oT1.."\n");
					file:flush();
				end
			end
			file:write("        	</p>\n");
			file:write("        </triangles>\n");
		end
		currMaterial = currMaterial:GetNextSibling();
	end
	file:write("      </mesh>\n");
	file:write("    </geometry>\n");
	file:flush();
	
end

function main()
	local mainModel = editChar:GetModel();
	local realChar = editChar:GetCharacter();
	
	file:write("<?xml version='1.0' encoding='utf-8'?>\n");
	file:write("<COLLADA xmlns='http://www.collada.org/2005/11/COLLADASchema' version='1.4.1'>\n");
	file:write("  <asset>\n");
	file:write("    <contributor>\n");
	file:write("      <author>Put your name</author>\n");
	file:write("      <authoring_tool>fragMOTION</authoring_tool>\n");
	file:write("      <comments>Exported from script version 4.2</comments>\n");
	file:write("    </contributor>\n");
	file:write("    <created>"..os.date("%Y-%m-%dT%H:%M:%SZ").."</created>\n");
	file:write("    <modified>"..os.date("%Y-%m-%dT%H:%M:%SZ").."</modified>\n");
	file:write("    <unit name='meter' meter='1.0'/>\n");
	file:write("    <up_axis>Y_UP</up_axis>\n");
	file:write("  </asset>\n");
	--Added a simple light
	file:write("  <library_lights>\n");
	file:write("    <light id='DEFAULT_AMBIENT_LIGHT' name='DEFAULT_AMBIENT_LIGHT'>\n");
	file:write("      <technique_common>\n");
	file:write("        <ambient>\n");
	file:write("          <color>1.0 1.0 1.0</color>\n");
	file:write("        </ambient>\n");
	file:write("      </technique_common>\n");
	file:write("    </light>\n");
	file:write("  </library_lights>\n");
	--Exort textures
	print("Exporting Textures");
	file:write("  <library_images>\n");
	allTextures = mainModel:GetTextures(0);		--Get all textures
	currTexture = allTextures:GetFirstChild();	--get first child, we will iterate using the siblings, the same will apply for most of the iterations
	while currTexture:isSet() do
		file:write("		<image id='T_"..string.gsub(currTexture:GetName()," ","_").."' name='"..string.gsub(currTexture:GetName()," ","_").."'>\n");
		trueTexture = mainModel:GetTextureByID(currTexture:GetID());
		file:write("		<init_from>file:///"..string.gsub(trueTexture.Path,"\\","/").."</init_from>\n");
		file:write("	</image>\n");
		currTexture = currTexture:GetNextSibling();
	end
	file:write("  </library_images>\n");
	file:flush();
	--Export materials
	print("Exporting Materials");
	file:write("  <library_materials>\n");
	allMaterials = mainModel:GetMaterials(0);
	currMaterial = allMaterials:GetFirstChild();
	while currMaterial:isSet() do
		file:write("    <material id='"..string.gsub(currMaterial:GetName()," ","_").."' name='"..string.gsub(currMaterial:GetName()," ","_").."'>\n");
		file:write("      <instance_effect url='#FX_"..string.gsub(currMaterial:GetName()," ","_").."'/>\n");
		file:write("    </material>\n");
		currMaterial = currMaterial:GetNextSibling();
	end
	file:write("  </library_materials>\n");
	file:flush();
	--Export effects (one per material), this is the part where the properties are set
	file:write("  <library_effects>\n");
	currMaterial = allMaterials:GetFirstChild();
	while currMaterial:isSet() do
		--Export diffuse and bump map texture
		currTexture = mainModel:GetMaterialByID(currMaterial:GetID()):GetTextureFromLayerByType(0); --color
		currBumpTexture = mainModel:GetMaterialByID(currMaterial:GetID()):GetTextureFromLayerByType(3); --Bum map
		
		file:write("    <effect id='FX_"..string.gsub(currMaterial:GetName()," ","_").."' name='FX_"..string.gsub(currMaterial:GetName()," ","_").."'>\n");
		file:write("      <profile_COMMON>\n");
		--Add a surface and a sampler per texture,
		if currTexture:isSet() then
			file:write("		<newparam sid='SURF_"..string.gsub(currTexture.Name," ","_").."'><surface type='2D'><init_from>T_"..string.gsub(currTexture.Name," ","_").."</init_from></surface></newparam>\n");
			file:write("		<newparam sid='S2D_"..string.gsub(currTexture.Name," ","_").."'><sampler2D><source>SURF_"..string.gsub(currTexture.Name," ","_").."</source></sampler2D></newparam>\n");
		end
		if currBumpTexture:isSet() then
			file:write("		<newparam sid='SURF_"..string.gsub(currBumpTexture.Name," ","_").."'><surface type='2D'><init_from>T_"..string.gsub(currBumpTexture.Name," ","_").."</init_from></surface></newparam>\n");
			file:write("		<newparam sid='S2D_"..string.gsub(currBumpTexture.Name," ","_").."'><sampler2D><source>SURF_"..string.gsub(currBumpTexture.Name," ","_").."</source></sampler2D></newparam>\n");
		end
		file:write("		<technique sid='TECH_"..string.gsub(currMaterial:GetName()," ","_").."'>\n");
		file:write("			<phong>\n");
		--The materials are in OLE_COLOR, we need the bit library to shift and add a mask to retrive the RGB componentes for each color. Each value is divided by 255
		local colorMask   = bitLib.Hex('FF');
		local emiColor = currMaterial.Emissive;
		local ambColor = currMaterial.Ambient;
		local diffColor = currMaterial.Diffuse;
		local specColor = currMaterial.Specular;

		file:write("				<emission><color>"..formatFloat(bitLib.And(bitLib.ShiftR(emiColor,0),colorMask)/255).." "..formatFloat(bitLib.And(bitLib.ShiftR(emiColor,8),colorMask)/255).." "..formatFloat(bitLib.And(bitLib.ShiftR(emiColor,16),colorMask)/255).." 1.0</color></emission>\n");
		file:write("				<ambient><color>"..formatFloat(bitLib.And(bitLib.ShiftR(ambColor,0),colorMask)/255).." "..formatFloat(bitLib.And(bitLib.ShiftR(ambColor,8),colorMask)/255).." "..formatFloat(bitLib.And(bitLib.ShiftR(ambColor,16),colorMask)/255).." 1.0</color></ambient>\n");
		file:write("				<diffuse>\n");
		--COLLADA only allows texture or color, no both
		if currTexture:isSet() then
			file:write("					<texture texture='S2D_"..string.gsub(currTexture.Name," ","_").."' texcoord='CHANNEL0'/>\n");
		else
			file:write("					<color>"..formatFloat(bitLib.And(bitLib.ShiftR(diffColor,0),colorMask)/255).." "..formatFloat(bitLib.And(bitLib.ShiftR(diffColor,8),colorMask)/255).." "..formatFloat(bitLib.And(bitLib.ShiftR(diffColor,16),colorMask)/255).." 1.0</color>\n");		
		end
		file:write("				</diffuse>\n");
		file:write("				<specular><color>"..formatFloat(bitLib.And(bitLib.ShiftR(specColor,0),colorMask)/255).." "..formatFloat(bitLib.And(bitLib.ShiftR(specColor,8),colorMask)/255).." "..formatFloat(bitLib.And(bitLib.ShiftR(specColor,16),colorMask)/255).." 1.0</color></specular>\n");
		file:write("			</phong>\n");
		--If bump map is present we use the FCOLLADA extension, it's not supported by all the programs
		if currBumpTexture:isSet() then
			file:write("			<extra>\n");
			file:write("				<technique profile='FCOLLADA'>\n");
			file:write("					<bump>\n");
			file:write("						<texture texture='S2D_"..string.gsub(currBumpTexture.Name," ","_").."' texcoord='uv0'/>\n");
			file:write("					</bump>\n");
			file:write("				</technique>\n");
			file:write("			</extra>\n");
		end
		file:write("		</technique>\n");
		file:write("      </profile_COMMON>\n");
		file:write("    </effect>\n");
		currMaterial = currMaterial:GetNextSibling();
	end
	file:write("  </library_effects>\n");
	file:flush();

	file:write("  <library_geometries>\n");
	
	--Export the mesh, 
	print("Exporting Mesh");
	
	local myMesh = editChar:GetDefaultMesh();
	ExportMesh(myMesh, "");
	local verts, vert_count, faces, face_count = myMesh:LockMesh();
	myMesh:UnlockMesh();
	
	--[[
	print("Exporting Morph targets");

	local animMesh = IMeshAnimation(myMesh);
	local allAnims = realChar:GetAnimations();
	local currAnim = allAnims:GetFirstChild();

	if(animMesh:isSet()) then

		while currAnim:isSet() do
			local meshKList = animMesh:GetMeshKeyList(IAnimation(currAnim));
			if(meshKList:isSet()) then
				meshKList:foreach("key", process_keys);
			end
			currAnim = currAnim:GetNextSibling();
		end
	end
	--]]
	
	file:write("  </library_geometries>\n");
	
	--This will indicate how to bind the mesh to the skeleton
	print("Binding the skeleton");
	file:write("  <library_controllers>\n");
	file:write("    <controller id='SKIN_"..string.gsub(myMesh.Name," ","_").."'>\n");
	--If there are morph targets we need to skin the morph controller
	local skinnedMeshName = "MESH_LIB_"..string.gsub(myMesh.Name," ","_")
	if(globalKeyCounter > 0 ) then
		skinnedMeshName = "MORPH_TARGET"	
	end
	file:write("      <skin source='#"..skinnedMeshName.."'>\n");
	file:write("        <bind_shape_matrix>1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1</bind_shape_matrix>\n");
	file:write("        <source id='JOINTS_"..string.gsub(myMesh.Name," ","_").."'>\n");
	
	strJoints = "";
	procJoints = 0;
	--The ID in the skeleton could not be sequential, we need a table to store the new indexes in the file
	bonesTable = {};
	while procJoints < mySkeleton:BoneTotal() do
		currentBone = mySkeleton:GetBoneByID(procJoints + 1);
		if currentBone:isSet() then
			strJoints = strJoints..string.gsub(string.gsub(currentBone.Name," ","_"),"mixamorig:","").." ";
			bonesTable[currentBone.ID] = procJoints;
			procJoints = procJoints + 1;
		end 
	end
	
	file:write("          <Name_array id='JOINTS_ARRAY_"..string.gsub(myMesh.Name," ","_").."' count='"..mySkeleton:BoneTotal().."'>"..strJoints.."</Name_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#JOINTS_ARRAY_"..string.gsub(myMesh.Name," ","_").."' count='"..mySkeleton:BoneTotal().."' stride='1'>\n");
	file:write("              <param name='JOINT' type='name'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");
	file:write("        <source id='BIND_POSES_"..string.gsub(myMesh.Name," ","_").."'>\n");
	
	
	strPosesArray = "";
	procJoints = 0;
	local parentBone;
	
	--Get the inverse bind matrix for each bone
	

	local mtxLocal = matrix_Array();
   local boneMap = BOOL_Array();
	mySkeleton:BuildLocalTransformList(mtxLocal,boneMap,256);
	
	file:write("          <float_array id='BIND_POSES_ARRAY_"..string.gsub(myMesh.Name," ","_").."' count='"..(mySkeleton:BoneTotal()*16).."'>\n");

	while procJoints < mySkeleton:BoneTotal() do
		procJoints = procJoints + 1; 
		local bindMatrix = mtxLocal[procJoints];
		local determ = 1;
		bindMatrix, determ = matrixLib.inverse(bindMatrix, determ);
		bindMatrix = matrixLib.transpose(bindMatrix);

		strPosesArray = "";
		for i = 0, 15, 1 do
			strPosesArray = strPosesArray..formatFloat(bindMatrix.m[i]).." ";
		end
		file:write(strPosesArray.."\n");
	end
	
	file:write("          </float_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#BIND_POSES_ARRAY_"..string.gsub(myMesh.Name," ","_").."' count='"..mySkeleton:BoneTotal().."' stride='16'>\n");
	file:write("              <param name='TRANSFORM' type='float4x4'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");
	file:write("        <source id='WEIGHTS_"..string.gsub(myMesh.Name," ","_").."'>\n");

	--Retrieve the weights array
	strVert = ""
	countWeights = 0;
	for i = 0,vert_count-1,1 do
		if verts[i].matrixIndex[0]~=0 then
			countWeights = countWeights + 1;
		end 
		if verts[i].matrixIndex[1]~=0 then
			countWeights = countWeights + 1;
		end 
		if verts[i].matrixIndex[2]~=0 then
			countWeights = countWeights + 1;
		end 
		if verts[i].matrixIndex[3]~=0 then
			countWeights = countWeights + 1;
		end		
	end
	file:write("          <float_array id='WEIGHTS_ARRAY_"..string.gsub(myMesh.Name," ","_").."' count='"..countWeights.."'>");

	for i = 0,vert_count-1,1 do
		if verts[i].matrixIndex[0]~=0 then
			file:write(verts[i].blend0.." ");
		end 
		if verts[i].matrixIndex[1]~=0 then
			file:write(verts[i].blend1.." ");
		end 
		if verts[i].matrixIndex[2]~=0 then
			file:write(verts[i].blend2.." ");
		end 
		if verts[i].matrixIndex[3]~=0 then
			file:write((1 - (verts[i].blend0 + verts[i].blend1 + verts[i].blend2 ) ).." "); --There are only 3 blend values, as the weight should be 1 we calculate the last weight 
		end		
	end

    file:write("          </float_array>\n");
	file:write("          <technique_common>\n");
	file:write("            <accessor source='#WEIGHTS_ARRAY_"..string.gsub(myMesh.Name," ","_").."' count='"..countWeights.."' stride='1'>\n");
	file:write("              <param name='WEIGHT' type='float'/>\n");
	file:write("            </accessor>\n");
	file:write("          </technique_common>\n");
	file:write("        </source>\n");
	file:write("        <joints>\n");
	file:write("          <input semantic='JOINT' source='#JOINTS_"..string.gsub(myMesh.Name," ","_").."'/>\n");
	file:write("          <input semantic='INV_BIND_MATRIX' source='#BIND_POSES_"..string.gsub(myMesh.Name," ","_").."'/>\n");
	file:write("        </joints>\n");
	file:write("        <vertex_weights count='"..vert_count.."'>\n");
	file:write("          <input semantic='JOINT' source='#JOINTS_"..string.gsub(myMesh.Name," ","_").."' offset='0'/>\n");
	file:write("          <input semantic='WEIGHT' source='#WEIGHTS_"..string.gsub(myMesh.Name," ","_").."' offset='1'/>\n");
	--Now we need to indicate the number of bones per vertex
    file:write("          <vcount>");
	strVert = ""
	for i = 0,vert_count-1,1 do
		numBones = 0
		if verts[i].matrixIndex[0]~=0 then
			numBones = numBones + 1
		end 
		if verts[i].matrixIndex[1]~=0 then
			numBones = numBones + 1
		end 
		if verts[i].matrixIndex[2]~=0 then
			numBones = numBones + 1
		end 
		if verts[i].matrixIndex[3]~=0 then
			numBones = numBones + 1
		end 
        file:write(numBones.." ");
	end	
	file:write("          </vcount>\n");
	--This is where the bones and weights information mix
	strVert = "";
	countWeights = 0;
    file:write("          <v>");
	for i = 0,vert_count-1,1 do
		for j=0,3,1 do
			if verts[i].matrixIndex[j]~=0 then
                file:write(bonesTable[verts[i].matrixIndex[j]].." "..countWeights.." ");
				countWeights = countWeights + 1;
			end 
		end
	end
	file:write("</v>\n");
	file:write("        </vertex_weights>\n");
	file:write("      </skin>\n");
	file:write("    </controller>\n");
	file:flush();
	
	--ADD controller for MORPHS
	
	if(globalKeyCounter>0) then
	
		local strMeshSufix = "MORPH_TARGET";
		file:write("	<controller id='"..strMeshSufix.."' >\n");
		file:write("		<morph source='#MESH_LIB_"..string.gsub(myMesh.Name," ","_").."' method='NORMALIZED'>\n");
		file:write("			<source id='TARGETS_"..strMeshSufix.."'>\n");
		file:write("				<IDREF_array id='TARGETS_ARRAY_"..strMeshSufix.."' count='"..globalKeyCounter.."'>");
		for xx = 1, globalKeyCounter, 1 do
			file:write("MESH_LIB_MORPH_"..xx.." ");
		end
		file:write("</IDREF_array>\n");
		file:write("				<technique_common>\n");
		file:write("					<accessor source='#TARGETS_ARRAY_"..strMeshSufix.."' count='"..globalKeyCounter.."' stride='1'>\n");
		file:write("						<param name='MORPH_TARGET' type='IDREF'/>\n");
		file:write("					</accessor>\n");
		file:write("				</technique_common>\n");
		file:write("			</source>\n");
		file:write("			<source id='MORPH_WEIGHTS_"..strMeshSufix.."'>\n");
		file:write("				<float_array id='MORPH_WEIGHTS_ARRAY_"..strMeshSufix.."' count='"..globalKeyCounter.."' >");
		for xx = 1, globalKeyCounter, 1 do
			file:write("0 ");
		end
		file:write("</float_array>\n");
		file:write("				<technique_common>\n");
		file:write("					<accessor source='#MORPH_WEIGHTS_ARRAY_"..strMeshSufix.."' count='"..globalKeyCounter.."' stride='1'>\n");
		file:write("						<param name='MORPH_WEIGHT' type='float'/>\n");
		file:write("					</accessor>\n");
		file:write("				</technique_common>\n");
		file:write("			</source>\n");
		file:write("			<targets>\n");
		file:write("				<input semantic='MORPH_TARGET' source='#TARGETS_"..strMeshSufix.."'/>\n");
		file:write("				<input semantic='MORPH_WEIGHT' source='#MORPH_WEIGHTS_"..strMeshSufix.."'/>\n");
		file:write("			</targets>\n");
		file:write("		</morph>\n");
		file:write("	</controller>\n");
	end
		
	file:write("  </library_controllers>\n");
	file:flush();
	
	print("Exporting Animation(s)");
	--If there is no skeleton there is no animation, sorry, no vertex animation
	local rootBone = mySkeleton:GetBoneByID(1);
	if rootBone:isSet() then
		local allAnimations = realChar:GetAnimations();
		local currentAnimation = allAnimations:GetFirstChild();
		--If there is a selected animation we export it, if no animation is selected we export all
		if pAnim:isSet() then
			currentAnimation = pAnim;
		end
		while currentAnimation:isSet() do
			print("Exporting Animation "..currentAnimation:GetName());
			local animName = string.gsub(currentAnimation:GetName()," ","_");
			local trueAnimation = realChar:GetAnimByID(currentAnimation:GetID());
			--Write the animation name
			file:write("  <library_animations id='"..animName.."'>\n");
			procJoints = 0;
			while procJoints < mySkeleton:BoneTotal() do
				--We will export for each bone the keyframes and the translation, rotation and scale
				currentBone = mySkeleton:GetBoneByID(procJoints + 1);
				if currentBone:isSet() then
					--Get the key list
					local animKeyList = trueAnimation:GetAnimKeyList(currentBone.ID);
					local framesList = animKeyList:GetFrameList(0);
					local currentFrame = 0;
					--Get current bone matrix
					local boneName = string.gsub( string.gsub(currentBone.Name," ","_"), "mixamorig:","");
					local bMatrix = IMatrix4x4(currentBone.LocalMatrix):GetMatrix();
					local timeFrames  = 0;
					local timeString = "\n";
					local translateString = "\n";
					local scaleString = "";
					local rotateXString = "\n";
					local rotateYString = "\n";
					local rotateZString = "\n";
					
					--We will export in seconds, so we need the FPS to make the conversion
					local numFPS = trueAnimation.FPS;

					currentFrame = framesList:GetFirstFrame();


					--while currentFrame >= 0 do
					for currentFrame=1, trueAnimation.NumFrames, 1 do
					
						--Get the transformation for this frame
						local matTransform = animKeyList:GetTransformByFrame(currentFrame);
						local translateX = 0;
						local translateY = 0;
						local translateZ = 0;
						local scaleX = 0;
						local scaleY = 0;
						local scaleZ = 0;
						
						--Convert the frame to seconds
						timeString = timeString..formatFloat((currentFrame - 1)/numFPS).." ";						
					
						--Apply the transformation to the bone
						matTransform = matrixLib.multiply(matTransform, bMatrix);
						
						--Get the translation
						translateX = matTransform.m[12];
						translateY = matTransform.m[13];
						translateZ = matTransform.m[14];
												
						translateString = translateString..formatFloat(translateX).." "..formatFloat(translateY).." "..formatFloat(translateZ).." \n";
						
						--Get the scale
						scaleX = math.sqrt(math.pow(matTransform.m[0],2) + math.pow(matTransform.m[1],2) + math.pow(matTransform.m[2],2));
						scaleY = math.sqrt(math.pow(matTransform.m[4],2) + math.pow(matTransform.m[5],2) + math.pow(matTransform.m[6],2));
						scaleZ = math.sqrt(math.pow(matTransform.m[8],2) + math.pow(matTransform.m[9],2) + math.pow(matTransform.m[10],2));

						scaleString = scaleString..formatFloat(scaleX).." "..formatFloat(scaleY).." "..formatFloat(scaleZ).." \n";						
						
						--Get the rotation
						local rotX,rotY,rotZ = getRotation(matTransform);
						rotateXString = rotateXString..formatFloat(math.deg(rotX)).." ";
						rotateYString = rotateYString..formatFloat(math.deg(rotY)).." ";
						rotateZString = rotateZString..formatFloat(math.deg(rotZ)).." ";
						
												
						if math.mod(currentFrame,16) == 0 then
      					timeString = timeString.."\n";						
							rotateXString = rotateXString.." \n";
							rotateYString = rotateYString.." \n";
							rotateZString = rotateZString.." \n";
						end
						
						timeFrames = timeFrames + 1;
					end
					
					--Write the translation information
					file:write("  	<animation id='"..animName.."_"..boneName.."_translate'>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_translate_input'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_translate_input_array' count='"..timeFrames.."'>"..timeString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_translate_input_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='TIME' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_translate_output'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_translate_output_array' count='"..(timeFrames * 3).."'>"..translateString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_translate_output_array' count='"..timeFrames.."' stride='3'>\n");
					file:write("  					<param name='X' type='float'/>\n");
					file:write("  					<param name='Y' type='float'/>\n");
					file:write("  					<param name='Z' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_translate_interpolations'>\n");
					file:write("  			<Name_array id='"..animName.."_"..boneName.."_translate_interpolations_array' count='"..timeFrames.."'>"..string.rep("LINEAR ", timeFrames).."</Name_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_translate_interpolations_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='INTERPOLATION' type='Name'/>\n");
					file:write("  				</accessor >\n");
					file:write("  			</technique_common>\n");
					file:write("  		</source>\n");
					file:write("  		<sampler id='"..animName.."_"..boneName.."_translate_sampler'>\n");
					file:write("  			<input semantic='INPUT' source='#"..animName.."_"..boneName.."_translate_input'/>\n");
					file:write("  			<input semantic='OUTPUT' source='#"..animName.."_"..boneName.."_translate_output'/>\n");
					file:write("  			<input semantic='INTERPOLATION' source='#"..animName.."_"..boneName.."_translate_interpolations'/>\n");
					file:write("  		</sampler>\n");
					file:write("  		<channel source='#"..animName.."_"..boneName.."_translate_sampler' target='SKELETON_"..boneName.."/translate'/>\n");
					file:write("  	</animation>\n");
					
					--Write the rotation around X
					file:write("  	<animation id='"..animName.."_"..boneName.."_rotateX'>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateX_input'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_rotateX_input_array' count='"..timeFrames.."'>"..timeString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateX_input_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='TIME' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateX_output'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_rotateX_output_array' count='"..timeFrames.."'>"..rotateXString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateX_output_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='ANGLE' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateX_interpolations'>\n");
					file:write("  			<Name_array id='"..animName.."_"..boneName.."_rotateX_interpolations_array' count='"..timeFrames.."'>"..string.rep("LINEAR ", timeFrames).."</Name_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateX_interpolations_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='INTERPOLATION' type='Name'/>\n");
					file:write("  				</accessor >\n");
					file:write("  			</technique_common>\n");
					file:write("  		</source>\n");
					file:write("  		<sampler id='"..animName.."_"..boneName.."_rotateX_sampler'>\n");
					file:write("  			<input semantic='INPUT' source='#"..animName.."_"..boneName.."_rotateX_input'/>\n");
					file:write("  			<input semantic='OUTPUT' source='#"..animName.."_"..boneName.."_rotateX_output'/>\n");
					file:write("  			<input semantic='INTERPOLATION' source='#"..animName.."_"..boneName.."_rotateX_interpolations'/>\n");
					file:write("  		</sampler>\n");
					file:write("  		<channel source='#"..animName.."_"..boneName.."_rotateX_sampler' target='SKELETON_"..boneName.."/rotateX.ANGLE'/>\n");
					file:write("  	</animation>\n");
					--Write the rotation around Y
					file:write("  	<animation id='"..animName.."_"..boneName.."_rotateY'>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateY_input'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_rotateY_input_array' count='"..timeFrames.."'>"..timeString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateY_input_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='TIME' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateY_output'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_rotateY_output_array' count='"..timeFrames.."'>"..rotateYString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateY_output_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='ANGLE' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateY_interpolations'>\n");
					file:write("  			<Name_array id='"..animName.."_"..boneName.."_rotateY_interpolations_array' count='"..timeFrames.."'>"..string.rep("LINEAR ", timeFrames).."</Name_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateY_interpolations_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='INTERPOLATION' type='Name'/>\n");
					file:write("  				</accessor >\n");
					file:write("  			</technique_common>\n");
					file:write("  		</source>\n");
					file:write("  		<sampler id='"..animName.."_"..boneName.."_rotateY_sampler'>\n");
					file:write("  			<input semantic='INPUT' source='#"..animName.."_"..boneName.."_rotateY_input'/>\n");
					file:write("  			<input semantic='OUTPUT' source='#"..animName.."_"..boneName.."_rotateY_output'/>\n");
					file:write("  			<input semantic='INTERPOLATION' source='#"..animName.."_"..boneName.."_rotateY_interpolations'/>\n");
					file:write("  		</sampler>\n");
					file:write("  		<channel source='#"..animName.."_"..boneName.."_rotateY_sampler' target='SKELETON_"..boneName.."/rotateY.ANGLE'/>\n");
					file:write("  	</animation>\n");
					--Write the rotation around Z
					file:write("  	<animation id='"..animName.."_"..boneName.."_rotateZ'>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateZ_input'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_rotateZ_input_array' count='"..timeFrames.."'>"..timeString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateZ_input_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='TIME' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateZ_output'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_rotateZ_output_array' count='"..timeFrames.."'>"..rotateZString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateZ_output_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='ANGLE' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_rotateZ_interpolations'>\n");
					file:write("  			<Name_array id='"..animName.."_"..boneName.."_rotateZ_interpolations_array' count='"..timeFrames.."'>"..string.rep("LINEAR ", timeFrames).."</Name_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_rotateZ_interpolations_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='INTERPOLATION' type='Name'/>\n");
					file:write("  				</accessor >\n");
					file:write("  			</technique_common>\n");
					file:write("  		</source>\n");
					file:write("  		<sampler id='"..animName.."_"..boneName.."_rotateZ_sampler'>\n");
					file:write("  			<input semantic='INPUT' source='#"..animName.."_"..boneName.."_rotateZ_input'/>\n");
					file:write("  			<input semantic='OUTPUT' source='#"..animName.."_"..boneName.."_rotateZ_output'/>\n");
					file:write("  			<input semantic='INTERPOLATION' source='#"..animName.."_"..boneName.."_rotateZ_interpolations'/>\n");
					file:write("  		</sampler>\n");
					file:write("  		<channel source='#"..animName.."_"..boneName.."_rotateZ_sampler' target='SKELETON_"..boneName.."/rotateZ.ANGLE'/>\n");
					file:write("  	</animation>\n");

					--Write the scale information
					file:write("  	<animation id='"..animName.."_"..boneName.."_scale'>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_scale_input'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_scale_input_array' count='"..timeFrames.."'>"..timeString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_scale_input_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='TIME' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_scale_output'>\n");
					file:write("  			<float_array id='"..animName.."_"..boneName.."_scale_output_array' count='"..(timeFrames * 3).."'>"..scaleString.."</float_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_scale_output_array' count='"..timeFrames.."' stride='3'>\n");
					file:write("  					<param name='X' type='float'/>\n");
					file:write("  					<param name='Y' type='float'/>\n");
					file:write("  					<param name='Z' type='float'/>\n");
					file:write("  				</accessor>\n");
					file:write("  			</technique_common >\n");
					file:write("  		</source>\n");
					file:write("  		<source id='"..animName.."_"..boneName.."_scale_interpolations'>\n");
					file:write("  			<Name_array id='"..animName.."_"..boneName.."_scale_interpolations_array' count='"..timeFrames.."'>"..string.rep("LINEAR ", timeFrames).."</Name_array>\n");
					file:write("  			<technique_common>\n");
					file:write("  				<accessor source='#"..animName.."_"..boneName.."_scale_interpolations_array' count='"..timeFrames.."' stride='1'>\n");
					file:write("  					<param name='INTERPOLATION' type='Name'/>\n");
					file:write("  				</accessor >\n");
					file:write("  			</technique_common>\n");
					file:write("  		</source>\n");
					file:write("  		<sampler id='"..animName.."_"..boneName.."_scale_sampler'>\n");
					file:write("  			<input semantic='INPUT' source='#"..animName.."_"..boneName.."_scale_input'/>\n");
					file:write("  			<input semantic='OUTPUT' source='#"..animName.."_"..boneName.."_scale_output'/>\n");
					file:write("  			<input semantic='INTERPOLATION' source='#"..animName.."_"..boneName.."_scale_interpolations'/>\n");
					file:write("  		</sampler>\n");
					file:write("  		<channel source='#"..animName.."_"..boneName.."_scale_sampler' target='SKELETON_"..boneName.."/scale'/>\n");
					file:write("  	</animation>\n");
					
					procJoints = procJoints + 1;
				end 
			end
			file:write("  </library_animations>\n");
			--If an animation is selected we end the loop
			if pAnim:isSet() then
				break;
			end
			currentAnimation = currentAnimation:GetNextSibling();
		end	
	end
	file:flush();

	print("Exporting Animation Clip(s)");
	--If there is no skeleton there is no animation clips, sorry, no vertex animation
	local rootBone = mySkeleton:GetBoneByID(1);
	local realChar = editChar:GetCharacter();
	if rootBone:isSet() then
		local allAnimations = realChar:GetAnimations();
		local currentAnimation = allAnimations:GetFirstChild();
		--If there is a selected animation we export it, if no animation is selected we export all

		if pAnim:isSet() then
			currentAnimation = pAnim;
		end
		file:write("  <library_animation_clips>\n");
		
		while currentAnimation:isSet() do

			--Get animation name 
			local animName = string.gsub(currentAnimation:GetName()," ","_");
			local trueAnimation = realChar:GetAnimByID(currentAnimation:GetID());
			local clipName = animName.."_CLIP";

			if trueAnimation.Looping == true then
				clipName = clipName.."-loop";
			end

			--Write the animation name, start and end time			
			file:write("  	<animation_clip id='"..clipName.."' start='0.0' end='"..formatFloat(trueAnimation.Duration).."'>\n");
			
			procJoints = 0;
			while procJoints < mySkeleton:BoneTotal() do
				--We will instantiate the translation, rotation and scale
				currentBone = mySkeleton:GetBoneByID(procJoints + 1);
				if currentBone:isSet() then
					local boneName = string.gsub( string.gsub(currentBone.Name," ","_"), "mixamorig:","");
					file:write("  		<instance_animation url='#"..animName.."_"..boneName.."_translate' />\n");
					file:write("  		<instance_animation url='#"..animName.."_"..boneName.."_rotateX' />\n");
					file:write("  		<instance_animation url='#"..animName.."_"..boneName.."_rotateY' />\n");
					file:write("  		<instance_animation url='#"..animName.."_"..boneName.."_rotateZ' />\n");
					file:write("  		<instance_animation url='#"..animName.."_"..boneName.."_scale' />\n");
					
					procJoints = procJoints + 1;
				end 
			end
			file:write("  	</animation_clip>\n");
			--If an animation is selected we end the loop
			if pAnim:isSet() then
				break;
			end
			currentAnimation = currentAnimation:GetNextSibling();
		end
		file:write("  </library_animation_clips>\n");	
	end
	file:flush();

	--This part instantiates all what we have created, if they are not instatiated they will not be exported
	file:write("  <library_visual_scenes>\n");	
	file:write("    <visual_scene id='RootNode' name='RootNode'>\n");
	--Instantiate the light
	file:write("      	<node id='SCENE_DEFAULT_LIGHT' name='ambientLight' type='NODE'>\n");
	file:write("        	<translate sid='translate'>100.0 100.0 100.0</translate>\n");
	file:write("	       	<lookat>100.0 100.0 100.0 0.0 0.0 0.0 0.0 1.0 0.0</lookat>\n");
	file:write("	       	<rotate sid='rotateZ'>0 0 1 0</rotate>\n");
	file:write("    	   	<rotate sid='rotateY'>0 1 0 0</rotate>\n");
	file:write("        	<rotate sid='rotateX'>1 0 0 0</rotate>\n");
	file:write("        	<instance_light url='#DEFAULT_AMBIENT_LIGHT'/>\n");
	file:write("      	</node>\n");
	print("Instantiating");
	--If we are exporting animation we need to instantiate the controller, if not only the mesh is instantiated
	if rootBone:isSet() then
		file:write("        <node id='"..string.gsub(myMesh.Name," ","_").."' name='MESH_NODE_NAME_"..string.gsub(myMesh.Name," ","_").."'>\n");
		file:write("          <instance_controller url='#SKIN_"..string.gsub(myMesh.Name," ","_").."'>\n");
		file:write("          <skeleton>#SKELETON_"..string.gsub(string.gsub(rootBone.Name," ","_"),"mixamorig:","").."</skeleton>\n");
		file:write("            <bind_material>\n");
		file:write("              <technique_common>\n");
		currMaterial = allMaterials:GetFirstChild();
		--We need to bind the materials
		while currMaterial:isSet() do
			file:write("                <instance_material symbol='"..string.gsub(currMaterial:GetName()," ","_").."' target='#"..string.gsub(currMaterial:GetName()," ","_").."'>\n");
			file:write("					<bind_vertex_input semantic='CHANNEL0' input_semantic='TEXCOORD' input_set='1' />\n");
			file:write("				</instance_material>\n");
			currMaterial = currMaterial:GetNextSibling();
		end
		file:write("              </technique_common>\n");
		file:write("            </bind_material>\n");
		file:write("          </instance_controller>\n");
		file:write("        </node>\n");
		file:write("        <node id='"..string.gsub(myMesh.Name," ","_").."_deformation_rig' name='deformation_rig' type='NODE'>\n");
		file:write("        	<rotate sid='rotateZ'>0 0 1 0</rotate>\n");
		file:write("        	<rotate sid='rotateY'>0 1 0 0</rotate>\n");
		file:write("        	<rotate sid='rotateX'>1 0 0 0</rotate>\n");
		--Write the skeleton
		printSkeleton(rootBone, rootBone:BoneCount(), 1);
		file:write("        </node>\n");
	else
		file:write("        <node id='"..string.gsub(myMesh.Name," ","_").."' name='MESH_NODE_NAME_"..string.gsub(myMesh.Name," ","_").."'>\n");
		file:write("          <instance_geometry url='#MESH_LIB_"..string.gsub(myMesh.Name," ","_").."'>\n");
		file:write("            <bind_material>\n");
		file:write("              <technique_common>\n");
		--We need to bind the materials
		currMaterial = allMaterials:GetFirstChild();
		while currMaterial:isSet() do
			file:write("                <instance_material symbol='"..string.gsub(currMaterial:GetName()," ","_").."' target='#"..string.gsub(currMaterial:GetName()," ","_").."'>\n");
			file:write("					<bind_vertex_input semantic='CHANNEL0' input_semantic='TEXCOORD' input_set='1' />\n");
			file:write("				</instance_material>\n");
			currMaterial = currMaterial:GetNextSibling();
		end
		file:write("              </technique_common>\n");
		file:write("            </bind_material>\n");
		file:write("          </instance_geometry>\n");
		file:write("        </node>\n");
	end
	file:write("    </visual_scene>\n");
	file:write("  </library_visual_scenes>\n");
	file:write("  <scene>\n");
	file:write("    <instance_visual_scene url='#RootNode'/>\n");
	file:write("  </scene>\n");
	file:write("</COLLADA>\n");
	file:close()
end

--Get the rotation from the matrix
function getRotation(mtxTransform)
	local rX,rY,rZ
	if (mtxTransform.m[2] ~= 1 and mtxTransform.m[2] ~= -1) then
		rY = -math.asin(mtxTransform.m[2]);
		rX = math.atan2(mtxTransform.m[6]/math.cos(rY), mtxTransform.m[10]/math.cos(rY));
		rZ = math.atan2(mtxTransform.m[1]/math.cos(rY), mtxTransform.m[0]/math.cos(rY));
	else
		rZ = 0;
		if (mtxTransform.m[2] == -1) then
			rY = math.pi/2;
			rX = math.atan2(mtxTransform.m[4], mtxTransform.m[8]);
		else
			rY = -math.pi/2;
			rX = math.atan2(-mtxTransform.m[4], -mtxTransform.m[8]);
		end
	end					
	return rX, rY, rZ
end
function formatFloat(number)
	return string.format("%.6f",number);
end
--Prints the current bone
function printSkeleton(currBone, numChilds, numTabs)
	local boneName = string.gsub( string.gsub(currBone.Name," ","_"), "mixamorig:","")
	file:write(string.rep("	", numTabs).."        <node id='SKELETON_"..boneName.."' name='SKELETON_"..boneName.."' sid='"..boneName.."' type='JOINT'>\n");
	local bMatrix = IMatrix4x4(currBone.LocalMatrix):GetMatrix();
	local rotX, rotY, rotZ = getRotation(bMatrix);
	--Write the information
	file:write(string.rep("	", numTabs + 1).."        <translate sid='translate'>"..formatFloat(bMatrix.m[12]).." "..formatFloat(bMatrix.m[13]).." "..formatFloat(bMatrix.m[14]).."</translate>\n");
	file:write(string.rep("	", numTabs + 1).."        <rotate sid='rotateZ'>0 0 1 "..formatFloat(math.deg(rotZ)).."</rotate>\n");
	file:write(string.rep("	", numTabs + 1).."        <rotate sid='rotateY'>0 1 0 "..formatFloat(math.deg(rotY)).."</rotate>\n");
	file:write(string.rep("	", numTabs + 1).."        <rotate sid='rotateX'>1 0 0 "..formatFloat(math.deg(rotX)).."</rotate>\n");
	file:write(string.rep("	", numTabs + 1).."        <scale sid='scale'>"..formatFloat(bMatrix.m[0]).." "..formatFloat(bMatrix.m[5]).." "..formatFloat(bMatrix.m[10]).."</scale>\n");
	local countChilds = 0;
	local countBones = 1;
	--Call the function for every child
	while countChilds<numChilds do
		chldBone = mySkeleton:GetBoneByID(countBones);
		if chldBone:isSet() then
			if chldBone:GetParentBone():isSet() then
				if chldBone:GetParentBone().Name == currBone.Name then
					printSkeleton(chldBone, chldBone:BoneCount(), numTabs + 1);
					countChilds = countChilds + 1;
				end
			end
		end
		countBones = countBones + 1;
	end
	file:write(string.rep("	", numTabs).."        </node>\n");
end

editChar:ClearOutput();
print("starting");
strPath = editChar:GetCurrentPath();
file = nil;
globalKeyCounter = 0;
pAnim = IUserObject(editChar:GetSelAnimation());
if strPath ~= "" then
	strPath = string.gsub(strPath, ".ugh", "");
	--If there is a selected animation we export it, if no animation is selected we export all
	if (pAnim:isSet()) then
		strPath = strPath.."@"..pAnim.Name;
	end
	file = io.open(strPath..".dae", "w");
	mySkeleton = editChar:GetSkeleton();
	main();
else
	print("File was not previously saved, please save it before running the script");
end
print("end");
