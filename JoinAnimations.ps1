#Add-Type -Path .\MathNet.Numerics.4.7.0\lib\net40\MathNet.Numerics.dll
#[psobject].Assembly.GetType("System.Management.Automation.TypeAccelerators")::Add('M','MathNet.Numerics.LinearAlgebra.Matrix[Double]')
#[psobject].Assembly.GetType("System.Management.Automation.TypeAccelerators")::Add('V','MathNet.Numerics.LinearAlgebra.Double.DenseVector')

$paramFile = $args[0]
$outputFile = $args[1]
$firstBone = "Hips"
function Convert-XmlElementToString
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        $xml
    )

    $sw = [System.IO.StringWriter]::new()
    $xmlSettings = [System.Xml.XmlWriterSettings]::new()
    $xmlSettings.ConformanceLevel = [System.Xml.ConformanceLevel]::Fragment
    $xmlSettings.Indent = $true
    $xw = [System.Xml.XmlWriter]::Create($sw, $xmlSettings)
    $xml.WriteTo($xw)
    $xw.Close()
    return $sw.ToString()
}

if(Test-Path $paramFile)
{
    $hipsName = ""
    $pathString = Split-Path -Path $paramFile;
    $workDir = (Get-ChildItem -Path $pathString);
    $mainFile = Split-Path $paramFile -leaf;

    $xml = [xml](Get-Content (Join-Path $pathString $mainFile) -Raw);
    $ns = New-Object System.Xml.XmlNamespaceManager($xml.NameTable)
    $ns.AddNamespace("ns", $xml.DocumentElement.NamespaceURI)
    $animationNode = $xml.SelectSingleNode("//ns:library_animations", $ns)

    $animClipsNode = $xml.CreateElement("library_animation_clips");

    $visualScene = $xml.SelectSingleNode("//ns:visual_scene", $ns)
    $armature = $visualScene.SelectSingleNode("ns:node[@id='Armature']",$ns);
    if($null -ne $armature)
    {
        $firstBone = "Armature"
    }
    Write-Progress -Activity "Initialization" -Status "Done"
    foreach($file in $workDir)
    {
        if($file.Extension.ToUpper() -eq ".DAE")
        {
            $newClip = $xml.CreateElement("animation_clip");
            $newClip.SetAttribute("id", "CLIP_" + $file.BaseName);
            
            [float]$clipStartTime = 0.0;
            [float]$clipEndTime = 0.0;

            if( $file.name -ne $mainFile)
            {
                $newxml = [xml](Get-Content (Join-Path $pathString $file.name ) -Raw);
                $ns1 = New-Object System.Xml.XmlNamespaceManager($newxml.NameTable)
                $ns1.AddNamespace("ns", $newxml.DocumentElement.NamespaceURI)
                $newNode = $newxml.SelectSingleNode("//ns:library_animations", $ns1)
                $newNode.SetAttribute("id", $file.BaseName);
                
                $hips_animations = $newNode.SelectSingleNode("ns:animation[contains(@id,'"+ $firstBone +"')]", $ns1);
                $hipsName = $hips_animations.GetAttribute("name")
                $root_doc_animation = $hips_animations.Clone();

                $transforms_node = $hips_animations.SelectSingleNode(".//ns:float_array[contains(@id,'output')]", $ns1);
                $transforms_node_root = $root_doc_animation.SelectSingleNode(".//ns:float_array[contains(@id,'output')]", $ns1);
                
                $num_keys = [int]$transforms_node.GetAttribute("count") / 16;
                $keys_hips = $transforms_node.innerText.Trim() -split '\s+|\r\n';
                $keys_root = $transforms_node_root.innerText.Trim() -split '\s+|\r\n';
                
                for($keycount =0; $keycount -lt $num_keys; $keycount++)
                {
                    if($file.name.Contains("Idle") -or $file.name.Contains("Shot"))
                    {
                        #Set identity matrix for all, this will prevent moving when static (idle)
                        $keys_root[($keycount*16) + 0] = "1.000000";
                        $keys_root[($keycount*16) + 1] = "0.000000";
                        $keys_root[($keycount*16) + 2] = "0.000000";
                        $keys_root[($keycount*16) + 3] = "0.000000";

                        $keys_root[($keycount*16) + 4] = "0.000000";
                        $keys_root[($keycount*16) + 5] = "1.000000";
                        $keys_root[($keycount*16) + 6] = "0.000000";
                        $keys_root[($keycount*16) + 7] = "0.000000";

                        $keys_root[($keycount*16) + 8] = "0.000000";
                        $keys_root[($keycount*16) + 9] = "0.000000";
                        $keys_root[($keycount*16) + 10] = "1.000000";
                        $keys_root[($keycount*16) + 11] = "0.000000";

                        $keys_root[($keycount*16) + 12] = "0.000000";
                        $keys_root[($keycount*16) + 13] = "0.000000";
                        $keys_root[($keycount*16) + 14] = "0.000000";
                        $keys_root[($keycount*16) + 15] = "1.000000";
                    }
                    elseif($file.name.Contains("Turn"))
                    {
                        #Convert rotation matrix to quaternion
                        $tr = [double]$keys_root[($keycount*16) + 0] + [double]$keys_root[($keycount*16) + 5] + [double]$keys_root[($keycount*16) + 10] + 1;
                        if ($tr -gt 0) 
                        { 
                        $S = 0.5 / [math]::Sqrt($tr); # S=4*qw 
                        $qw = 0.25 / $S;
                        $qx = ([double]$keys_root[($keycount*16) + 9] - [double]$keys_root[($keycount*16) + 6]) * $S;
                        $qy = ([double]$keys_root[($keycount*16) + 2] - [double]$keys_root[($keycount*16) + 8]) * $S; 
                        $qz = ([double]$keys_root[($keycount*16) + 4] - [double]$keys_root[($keycount*16) + 1]) * $S; 
                        } 
                        elseif (([double]$keys_root[($keycount*16) + 0] -gt [double]$keys_root[($keycount*16) + 5]) -and  ([double]$keys_root[($keycount*16) + 0] -gt [double]$keys_root[($keycount*16) + 10])) 
                        { 
                        $S = [math]::Sqrt(1.0 + [double]$keys_root[($keycount*16) + 0] - [double]$keys_root[($keycount*16) + 5] - [double]$keys_root[($keycount*16) + 10]) * 2; # S=4*qx 
                        $qw = ([double]$keys_root[($keycount*16) + 6] + [double]$keys_root[($keycount*16) + 9]) / $S;
                        $qx = 0.5 / $S;
                        $qy = ([double]$keys_root[($keycount*16) + 4] + [double]$keys_root[($keycount*16) + 1]) / $S; 
                        $qz = ([double]$keys_root[($keycount*16) + 8] + [double]$keys_root[($keycount*16) + 2]) / $S; 
                        } 
                        elseif ([double]$keys_root[($keycount*16) + 5] -gt [double]$keys_root[($keycount*16) + 10]) 
                        { 
                        $S = [math]::Sqrt(1.0 + [double]$keys_root[($keycount*16) + 5] - [double]$keys_root[($keycount*16) + 0] - [double]$keys_root[($keycount*16) + 10]) * 2; # S=4*qy
                        $qw = ([double]$keys_root[($keycount*16) + 8] + [double]$keys_root[($keycount*16) + 2]) / $S;
                        $qx = ([double]$keys_root[($keycount*16) + 4] + [double]$keys_root[($keycount*16) + 1]) / $S; 
                        $qy = 0.5 / $S;
                        $qz = ([double]$keys_root[($keycount*16) + 9] + [double]$keys_root[($keycount*16) + 6]) / $S; 
                        } 
                        else 
                        { 
                        $S = [math]::Sqrt(1.0 + [double]$keys_root[($keycount*16) + 10] - [double]$keys_root[($keycount*16) + 0] - [double]$keys_root[($keycount*16) + 5]) * 2; # S=4*qz
                        $qw = ([double]$keys_root[($keycount*16) + 1] + [double]$keys_root[($keycount*16) + 4]) / $S;
                        $qx = ([double]$keys_root[($keycount*16) + 8] + [double]$keys_root[($keycount*16) + 2]) / $S;
                        $qy = ([double]$keys_root[($keycount*16) + 9] + [double]$keys_root[($keycount*16) + 6]) / $S;
                        $qz = 0.5 / $S;
                        }

                        #Get Euler angles from quaternion
                        $sqw = $qw*$qw;
                        $sqx = $qx*$qx;
                        $sqy = $qy*$qy;
                        $sqz = $qz*$qz;
                        $unit = $sqx + $sqy + $sqz + $sqw; # if normalised is one, otherwise is correction factor
                        $test = $qx*$qy + $qz*$qw;
                        if ($test -gt 0.499*$unit) 
                        { # singularity at north pole
                            $heading = 2 * [math]::Atan2($qx,$qw);
                            $attitude = [math]::PI/2;
                            $bank = 0;
                        }
                        elseif ($test -lt -0.499*$unit) 
                        { # singularity at south pole
                            $heading = -2 * [math]::Atan2($qx,$qw);
                            $attitude = -[math]::PI/2;
                            $bank = 0;
                        }
                        else {
                            $heading = [math]::atan2(2*$qy*$qw-2*$qx*$qz , $sqx - $sqy - $sqz + $sqw);
                            $attitude = [math]::asin(2*$test/$unit);
                            $bank = [math]::atan2(2*$qx*$qw-2*$qy*$qz , -$sqx + $sqy - $sqz + $sqw);
                        }

                        #Convert angles to Rotation Matrix
                        $st = [math]::sin($bank); # X
                        $ct = [math]::cos($bank);
                        $sb = [math]::sin($heading);# Y
                        $cb = [math]::cos($heading);
                        $sa = [math]::sin($attitude);# Z
                        $ca = [math]::cos($attitude);

                        $st0 = [math]::sin(0);
                        $ct0 = [math]::cos(0);
                        $sb0 = [math]::sin(0);
                        $cb0 = [math]::cos(0);
                        $sa0 = [math]::sin(0);
                        $ca0 = [math]::cos(0);

                        $keys_root[($keycount*16) + 0] = $ca0*$cb;
                        $keys_root[($keycount*16) + 1] = $ca0*$sb*$st0 - $sa0*$st0;
                        $keys_root[($keycount*16) + 2] = $ca0*$sb*$ct0 + $sa0*$st0;
                        $keys_root[($keycount*16) + 3] = "0.000000";

                        $keys_root[($keycount*16) + 4] = $sa0*$cb;
                        $keys_root[($keycount*16) + 5] = $sa0*$sb*$st0 + $ca0*$ct0;
                        $keys_root[($keycount*16) + 6] = $sa0*$sb*$ct0 - $ca0*$st0;
                        $keys_root[($keycount*16) + 7] = "0.000000";

                        $keys_root[($keycount*16) + 8] = -$sb;
                        $keys_root[($keycount*16) + 9] = $cb*$st0;
                        $keys_root[($keycount*16) + 10] = $cb*$ct0;
                        #$keys_root[($keycount*16) + 11] = "0.000000";

                        $keys_root[($keycount*16) + 12] = "0.000000";
                        $keys_root[($keycount*16) + 13] = "0.000000";
                        $keys_root[($keycount*16) + 14] = "0.000000";
                        $keys_root[($keycount*16) + 15] = "1.000000";


                        #Set rotation matrix, clean Z translation
                        $keys_hips[($keycount*16) + 0] = $ca*$cb0;
                        $keys_hips[($keycount*16) + 1] = $ca*$sb0*$st - $sa*$st;
                        $keys_hips[($keycount*16) + 2] = $ca*$sb0*$ct + $sa*$st;
                        #$keys_hips[($keycount*16) + 3] = "0.000000";

                        $keys_hips[($keycount*16) + 4] = $sa*$cb0;
                        $keys_hips[($keycount*16) + 5] = $sa*$sb0*$st + $ca*$ct;
                        $keys_hips[($keycount*16) + 6] = $sa*$sb0*$ct - $ca*$st;
                        #$keys_hips[($keycount*16) + 7] = "0.000000";

                        $keys_hips[($keycount*16) + 8] = -$sb0;
                        $keys_hips[($keycount*16) + 9] = $cb0*$st;
                        $keys_hips[($keycount*16) + 10] = $cb0*$ct;
                        $keys_hips[($keycount*16) + 11] = "0.000000";

                        $keys_hips[($keycount*16) + 12] = "0.000000";
                        $keys_hips[($keycount*16) + 13] = "0.000000";
                        $keys_hips[($keycount*16) + 14] = "0.000000";
                        $keys_hips[($keycount*16) + 15] = "1.000000";
                    }
                    else 
                    {
                        #Clean all, except Z translation
                        $keys_root[($keycount*16) + 0] = "1.000000";
                        $keys_root[($keycount*16) + 1] = "0.000000";
                        $keys_root[($keycount*16) + 2] = "0.000000";
                        $keys_root[($keycount*16) + 3] = "0.000000";

                        $keys_root[($keycount*16) + 4] = "0.000000";
                        $keys_root[($keycount*16) + 5] = "1.000000";
                        $keys_root[($keycount*16) + 6] = "0.000000";
                        $keys_root[($keycount*16) + 7] = "0.000000";

                        $keys_root[($keycount*16) + 8] = "0.000000";
                        $keys_root[($keycount*16) + 9] = "0.000000";
                        $keys_root[($keycount*16) + 10] = "1.000000";
                        #$keys_root[($keycount*16) + 11] = "0.000000";

                        $keys_root[($keycount*16) + 12] = "0.000000";
                        $keys_root[($keycount*16) + 13] = "0.000000";
                        $keys_root[($keycount*16) + 14] = "0.000000";
                        $keys_root[($keycount*16) + 15] = "1.000000";

                        #Clean Z translation
                        #$keys_hips[($keycount*16) + 3] = "0.000000";
                        #$keys_hips[($keycount*16) + 7] = "0.000000";
                        $keys_hips[($keycount*16) + 11] = "0.000000";
                        
                    }
                }

                $transforms_node.InnerText = "$keys_hips";
                $transforms_node_root.InnerText = "$keys_root";

                $parsed_node = Convert-XmlElementToString $root_doc_animation;
                $parsed_node = $parsed_node -replace $hipsName, "root_doc"
                $new_node = [xml]$parsed_node;
                $newNode.InsertBefore($newxml.ImportNode($new_node.animation,$true) , $hips_animations) | Out-Null;
                
                $animationNodes = $newNode.SelectNodes("ns:animation", $ns1);
                foreach($aninNode in $animationNodes)
                {

                    $inputNode = $aninNode.SelectSingleNode("//ns:input[@semantic='INPUT']", $ns1)
                    if($inputNode)
                    {
                        $nameArray = $inputNode.GetAttribute("source").Substring(1);
                        $toFind = "//ns:source[@id='" + $nameArray + "']/ns:float_array";
                        $timeArray = $aninNode.SelectSingleNode($toFind, $ns1);
                        if($timeArray)
                        {
                            $times = $timeArray.innerText.Trim() -split '\s+|\r\n';
                            $newTime = [float]$times[-1];
                            if($newTime -gt $clipEndTime)
                            {
                                $clipEndTime = $newTime;
                            }
                        }

                    }
                    $aninNode.SetAttribute("id", $aninNode.GetAttribute("id") + "_" +  $file.BaseName);
                    #select all the source, float_array, Name_array and sampler nodes to change de ID
                    $sourceNodes = $aninNode.SelectNodes(".//ns:source | .//ns:float_array | .//ns:sampler | .//ns:Name_array", $ns1);
                    foreach($souceNode in $sourceNodes)
                    {
                        $souceNode.SetAttribute("id", $souceNode.GetAttribute("id") + "_" +  $file.BaseName);
                    }
                    #select all the accessor, channel and input nodes to change the source
                    $accesorNodes = $aninNode.SelectNodes(".//ns:accessor | .//ns:channel | .//ns:input", $ns1);
                    foreach($accNode in $accesorNodes)
                    {
                        $accNode.SetAttribute("source", $accNode.GetAttribute("source") + "_" +  $file.BaseName);
                    }
                    $intanceAnimation = $xml.CreateElement("instance_animation");
                    $intanceAnimation.SetAttribute("url", "#" + $aninNode.GetAttribute("id"));
                    $newClip.AppendChild($intanceAnimation) | Out-Null;
                }
                $newClip.SetAttribute("start", $clipStartTime);
                $newClip.SetAttribute("end", $clipEndTime);
                $animClipsNode.AppendChild($newClip) | Out-Null;
                $animationNode.ParentNode.InsertAfter($xml.ImportNode($newNode,$true), $animationNode) | Out-Null;
            }
        }
    }
    Write-Progress -Activity "Processing animations" -Status "Done"

    $animationNode.ParentNode.InsertAfter($animClipsNode, $animationNode) | Out-Null;
    $morphArray = @()
    #Validate the morphs
    $controllersNode = $xml.SelectSingleNode("//ns:library_controllers", $ns)
    foreach($controller in $controllersNode.ChildNodes)
    {
        $skinNode = $controller.SelectSingleNode("ns:skin",$ns);
        if($skinNode)
        {
            $morphName =  $skinNode.GetAttribute("source").Substring(1) + "-morph";
            $morphController = $controllersNode.SelectSingleNode("ns:controller[@id='" + $morphName + "']",$ns); 
            if($morphController)
            {
                $morphController.FirstChild.SetAttribute("method", "NORMALIZED");
                $skinNode.SetAttribute("source", "#" + $morphName);
                $morphArray = $morphArray + $morphController.SelectSingleNode("ns:morph/ns:source/ns:IDREF_array",$ns).InnerText.Split(" ");
            }
        }
    }
    Write-Progress -Activity "Validate Morphs" -Status "Done"

    #Recalculate normals in morphs
    $geometriesNode = $xml.SelectSingleNode("//ns:library_geometries", $ns)
    $percentMorphs = 0;
    foreach($morphMesh in $morphArray)
    {
        $geometry = $geometriesNode.SelectSingleNode("ns:geometry[@id='" + $morphMesh + "']",$ns); 
        $polylist = $geometry.SelectSingleNode("ns:mesh/ns:polylist",$ns);
        if($null -ne $polylist)
        {
            $normalsSource = $polylist.SelectSingleNode("ns:input[@semantic='NORMAL']",$ns);
            $vtxLib = $geometry.SelectSingleNode("ns:mesh/ns:vertices/ns:input",$ns).GetAttribute("source").Replace("#","");
            $verticesArray = $geometry.SelectSingleNode("ns:mesh/ns:source[@id='"+ $vtxLib +"']/ns:float_array",$ns).InnerText.Trim() -split '\s+|\r\n';

            $normals = $geometry.SelectSingleNode("ns:mesh/ns:source[@id='"+ $normalsSource.GetAttribute("source").Replace("#","") +"']/ns:float_array",$ns);

            $vcountArray = $polylist.SelectSingleNode("ns:vcount",$ns).InnerText.Trim() -split '\s+|\r\n';
            $pArray = $polylist.SelectSingleNode("ns:p",$ns).InnerText.Trim() -split '\s+|\r\n';
            $newpArray = [int[]]::new($vcountArray.Count * 3);;
            
            $newpNormalsArray = [float[]]::new($verticesArray.Count);
            $finalpNormalsArray = [float[]]::new($vcountArray.Count *3 * 3);

            $poffset = 0;
            for($vxs = 0; $vxs-lt $vcountArray.Count; $vxs++)
            {
                $vidx1 = $pArray[($poffset * 3) + 0] -as [int];
                $vidx2 = $pArray[($poffset * 3) + 3] -as [int];
                $vidx3 = $pArray[($poffset * 3) + 6] -as [int];

                #get two edges
                $vtx1x = $verticesArray[($vidx1 * 3) + 0];
                $vtx1y = $verticesArray[($vidx1 * 3) + 1];
                $vtx1z = $verticesArray[($vidx1 * 3) + 2];

                $vtx2x = $verticesArray[($vidx2 * 3) + 0];
                $vtx2y = $verticesArray[($vidx2 * 3) + 1];
                $vtx2z = $verticesArray[($vidx2 * 3) + 2];

                $vtx3x = $verticesArray[($vidx3 * 3) + 0];
                $vtx3y = $verticesArray[($vidx3 * 3) + 1];
                $vtx3z = $verticesArray[($vidx3 * 3) + 2];

                $ux = $vtx2x - $vtx1x;
                $uy = $vtx2y - $vtx1y;
                $uz = $vtx2z - $vtx1z;

                $vx = $vtx3x - $vtx1x;
                $vy = $vtx3y - $vtx1y;
                $vz = $vtx3z - $vtx1z;

                $nx = (($uy * $vz) - ($uz * $vy));
                $ny = (($uz * $vx) - ($ux * $vz));
                $nz = (($ux * $vy) - ($uy * $vx));

                $newpNormalsArray[($vidx1 * 3) + 0] = $newpNormalsArray[($vidx1 * 3) + 0] + $nx;
                $newpNormalsArray[($vidx1 * 3) + 1] = $newpNormalsArray[($vidx1 * 3) + 1] + $ny;
                $newpNormalsArray[($vidx1 * 3) + 2] = $newpNormalsArray[($vidx1 * 3) + 2] + $nz;

                $newpNormalsArray[($vidx2 * 3) + 0] = $newpNormalsArray[($vidx2 * 3) + 0] + $nx;
                $newpNormalsArray[($vidx2 * 3) + 1] = $newpNormalsArray[($vidx2 * 3) + 1] + $ny;
                $newpNormalsArray[($vidx2 * 3) + 2] = $newpNormalsArray[($vidx2 * 3) + 2] + $nz;

                $newpNormalsArray[($vidx3 * 3) + 0] = $newpNormalsArray[($vidx3 * 3) + 0] + $nx;
                $newpNormalsArray[($vidx3 * 3) + 1] = $newpNormalsArray[($vidx3 * 3) + 1] + $ny;
                $newpNormalsArray[($vidx3 * 3) + 2] = $newpNormalsArray[($vidx3 * 3) + 2] + $nz;

                #Add reference to the new normals
                $pArray[($poffset * 3) + 1] = $poffset + 0;
                $pArray[($poffset * 3) + 4] = $poffset + 1;
                $pArray[($poffset * 3) + 7] = $poffset + 2;

                $newpArray[($poffset ) + 0] = $vidx1;
                $newpArray[($poffset ) + 1] = $vidx2;
                $newpArray[($poffset ) + 2] = $vidx3;

                $poffset += 3; #Poly size, but for clarity it's always 3
            }

            #normalize normals
            for($z = 0; $z -lt $newpNormalsArray.Count ; $z+=3)
            {
                $p = [Math]::Pow($newpNormalsArray[$z + 0],2);
                $q = [Math]::Pow($newpNormalsArray[$z + 1],2);
                $r = [Math]::Pow($newpNormalsArray[$z + 2],2);
                $d = [Math]::Sqrt($p + $q + $r);
                if($d -gt 0)
                {
                    $newpNormalsArray[$z + 0] = $newpNormalsArray[$z + 0] / $d;
                    $newpNormalsArray[$z + 1] = $newpNormalsArray[$z + 1] / $d;
                    $newpNormalsArray[$z + 2] = $newpNormalsArray[$z + 2] / $d;
                }
            }


            #reassing normals
            $poffset = 0;
            for($vxs = 0; $vxs-lt $vcountArray.Count; $vxs++)
            {
                $vidx1 = $pArray[($poffset * 3) + 0] -as [int];
                $vidx2 = $pArray[($poffset * 3) + 3] -as [int];
                $vidx3 = $pArray[($poffset * 3) + 6] -as [int];

                $finalpNormalsArray[($poffset * 3) + 0] = $newpNormalsArray[($vidx1 * 3) + 0] + $nx;
                $finalpNormalsArray[($poffset * 3) + 1] = $newpNormalsArray[($vidx1 * 3) + 1] + $ny;
                $finalpNormalsArray[($poffset * 3) + 2] = $newpNormalsArray[($vidx1 * 3) + 2] + $nz;

                $finalpNormalsArray[($poffset * 3) + 3] = $newpNormalsArray[($vidx2 * 3) + 0] + $nx;
                $finalpNormalsArray[($poffset * 3) + 4] = $newpNormalsArray[($vidx2 * 3) + 1] + $ny;
                $finalpNormalsArray[($poffset * 3) + 5] = $newpNormalsArray[($vidx2 * 3) + 2] + $nz;

                $finalpNormalsArray[($poffset * 3) + 6] = $newpNormalsArray[($vidx3 * 3) + 0] + $nx;
                $finalpNormalsArray[($poffset * 3) + 7] = $newpNormalsArray[($vidx3 * 3) + 1] + $ny;
                $finalpNormalsArray[($poffset * 3) + 8] = $newpNormalsArray[($vidx3 * 3) + 2] + $nz;

                $poffset += 3;
            }


            $normals.InnerText = "$finalpNormalsArray";
            $normals.SetAttribute("count", $finalpNormalsArray.Count);

            $accesor = $geometry.SelectSingleNode("ns:mesh/ns:source[@id='"+ $normalsSource.GetAttribute("source").Replace("#","") +"']/ns:technique_common/ns:accessor",$ns);
            $accesor.SetAttribute("count", ($finalpNormalsArray.Count / 3));

            $polylist.SelectSingleNode("ns:p",$ns).InnerText = "$pArray";
        }
        $percentMorphs++;
        Write-Progress -Activity "Recalculate Normals" -PercentComplete (($percentMorphs/$morphArray.Count) * 100)
    }

    #Add new root node
    $hips = $visualScene.SelectSingleNode("ns:node[@id='" + $hipsName + "']",$ns);
    $hips_matrix = $hips.SelectSingleNode("ns:matrix",$ns);

    $matrix_values = $hips_matrix.innerText.Trim() -split '\s+|\r\n';
    
    $root_doc = $xml.CreateElement("node");
    $root_doc.SetAttribute("id", "root_doc");
    $root_doc.SetAttribute("name", "root_doc");
    $root_doc.SetAttribute("sid", "root_doc");
    $root_doc.SetAttribute("type", "JOINT");
    
    $root_matrix = $xml.CreateElement("matrix");
    $root_matrix.SetAttribute("sid", "matrix");
    $Xpos = $matrix_values[3];
    $Zpos = $matrix_values[11];

    if($file.name.Contains("Idle") -or $file.name.Contains("Shot"))
    {
        $root_matrix.innerText = "1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000";            
    }
    else 
    {
        $root_matrix.innerText = "1.000000 0.000000 0.000000 " + $Xpos + " 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 " + $Zpos + " 0.000000 0.000000 0.000000 1.000000";    
    }
    
    $root_doc.AppendChild($root_matrix) | Out-Null;

    if(!$file.name.Contains("Idle") -and !$file.name.Contains("Shot"))
    {
        $matrix_values[3] = "0.000000";
        $matrix_values[11] = "0.000000";
    }
    $hips_matrix.InnerText = "$matrix_values";

    $visualScene.RemoveChild($hips) | Out-Null;
    $root_doc.AppendChild($hips) | Out-Null;
    $visualScene.AppendChild($root_doc) | Out-Null;
    
    $skeleNodes = $visualScene.SelectNodes("//ns:skeleton", $ns);
    foreach($skeleNode in $skeleNodes)
    {
        $skeleNode.InnerText = "#root_doc";
    }

    Write-Progress -Activity "Adding root node" -Status "Done"

    #Modify binding matrices for skin controllers
    $skins =$xml.SelectNodes("//ns:skin", $ns);
    $countering = 0;
    foreach($skin in $skins)
    {
        $joints = $skin.SelectSingleNode("ns:source[contains(@id,'Joints')]", $ns);
        $matrices = $skin.SelectSingleNode("ns:source[contains(@id,'Matrices')]", $ns);
        
        $name_array = $joints.SelectSingleNode("ns:Name_array",$ns);
        $matrices_array = $matrices.SelectSingleNode("ns:float_array",$ns);

        $name_array.SetAttribute("count", [int]$name_array.GetAttribute("count") + 1);
        $name_array.innerText += " root_doc";
        $accessor = $joints.SelectSingleNode(".//ns:accessor", $ns1);
        $accessor.SetAttribute("count", [int]$accessor.GetAttribute("count") + 1);

        $matrices_array.SetAttribute("count", [int]$matrices_array.GetAttribute("count") + 16);
        $matrices_array.innerText += " 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000 0.000000 0.000000 0.000000 0.000000 1.000000";
        $accessor = $matrices.SelectSingleNode(".//ns:accessor", $ns1);
        $accessor.SetAttribute("count", [int]$accessor.GetAttribute("count") + 1);

        $countering++;
    }
    Write-Progress -Activity "Modifying skins" -Status "Done"
    $xml.Save($outputFile)
}
else 
{
    Write-Output "Invalid file name";
}
