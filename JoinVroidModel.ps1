$paramFile = $args[0]
$outputFile = $args[1]
$firstBone = "Hips"
function Convert-XmlElementToString
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        $xml
    )

    $sw = [System.IO.StringWriter]::new()
    $xmlSettings = [System.Xml.XmlWriterSettings]::new()
    $xmlSettings.ConformanceLevel = [System.Xml.ConformanceLevel]::Fragment
    $xmlSettings.Indent = $true
    $xw = [System.Xml.XmlWriter]::Create($sw, $xmlSettings)
    $xml.WriteTo($xw)
    $xw.Close()
    return $sw.ToString()
}

if(Test-Path $paramFile)
{
    $pathString = Split-Path -Path $paramFile;
    $workDir = (Get-ChildItem -Path $pathString);
    $mainFile = Split-Path $paramFile -leaf;

    $xml = [xml](Get-Content (Join-Path $pathString $mainFile) -Raw);
    $ns = New-Object System.Xml.XmlNamespaceManager($xml.NameTable)
    $ns.AddNamespace("ns", $xml.DocumentElement.NamespaceURI)
    $animationNode = $xml.SelectSingleNode("//ns:library_animations", $ns)

    $animClipsNode = $xml.CreateElement("library_animation_clips");

    #$visualScene = $xml.SelectSingleNode("//ns:visual_scene", $ns)

    Write-Progress -Activity "Initialization" -Status "Done"
    foreach($file in $workDir)
    {
        if($file.Extension.ToUpper() -eq ".DAE")
        {
            $newClip = $xml.CreateElement("animation_clip");
            $newClip.SetAttribute("id", "CLIP_" + $file.BaseName);
            
            [float]$clipStartTime = 0.0;
            [float]$clipEndTime = 0.0;

            if( $file.name -ne $mainFile)
            {
                $newxml = [xml](Get-Content (Join-Path $pathString $file.name ) -Raw);
                $ns1 = New-Object System.Xml.XmlNamespaceManager($newxml.NameTable)
                $ns1.AddNamespace("ns", $newxml.DocumentElement.NamespaceURI)
                $newNode = $newxml.SelectSingleNode("//ns:library_animations", $ns1)
                $newNode.SetAttribute("id", $file.BaseName);
                
                $hips_animations = $newNode.SelectSingleNode("ns:animation[contains(@id,'"+ $firstBone +"')]", $ns1);
                #$hipsName = $hips_animations.GetAttribute("name")
                $armature_animation = $newNode.SelectSingleNode("ns:animation[contains(@id,'Armature')]", $ns1);

                $transforms_node = $hips_animations.SelectSingleNode(".//ns:float_array[contains(@id,'output')]", $ns1);
                $transforms_node_root = $armature_animation.SelectSingleNode(".//ns:float_array[contains(@id,'output')]", $ns1);
                
                $num_keys = [int]$transforms_node.GetAttribute("count") / 16;
                $keys_hips = $transforms_node.innerText.Trim() -split '\s+|\r\n';
                $keys_root = $transforms_node_root.innerText.Trim() -split '\s+|\r\n';
                
                for($keycount =0; $keycount -lt $num_keys; $keycount++)
                {
                    if($file.name.Contains("Idle") -or $file.name.Contains("Shot"))
                    {
                        #don't change the matrices
                    }
                    else 
                    {
                        #Set X translation
                        $keys_root[($keycount*16) + 3] = ($keys_hips[($keycount*16) + 3] -as [float]) * 1;
                        #Clean X translation
                        $keys_hips[($keycount*16) + 3] = "0.000000";

                        #Set Z translation
                        $keys_root[($keycount*16) + 11] = ($keys_hips[($keycount*16) + 11] -as [float]) * 1;
                        #Clean Z translation
                        $keys_hips[($keycount*16) + 11] = "0.000000";
                    }
                }

                $transforms_node.InnerText = "$keys_hips";
                $transforms_node_root.InnerText = "$keys_root";

                $animationNodes = $newNode.SelectNodes("ns:animation", $ns1);
                foreach($aninNode in $animationNodes)
                {

                    $inputNode = $aninNode.SelectSingleNode("//ns:input[@semantic='INPUT']", $ns1)
                    if($inputNode)
                    {
                        $nameArray = $inputNode.GetAttribute("source").Substring(1);
                        $toFind = "//ns:source[@id='" + $nameArray + "']/ns:float_array";
                        $timeArray = $aninNode.SelectSingleNode($toFind, $ns1);
                        if($timeArray)
                        {
                            $times = $timeArray.innerText.Trim() -split '\s+|\r\n';
                            $newTime = [float]$times[-1];
                            if($newTime -gt $clipEndTime)
                            {
                                $clipEndTime = $newTime;
                            }
                        }

                    }
                    $aninNode.SetAttribute("id", $aninNode.GetAttribute("id") + "_" +  $file.BaseName);
                    #select all the source, float_array, Name_array and sampler nodes to change de ID
                    $sourceNodes = $aninNode.SelectNodes(".//ns:source | .//ns:float_array | .//ns:sampler | .//ns:Name_array", $ns1);
                    foreach($souceNode in $sourceNodes)
                    {
                        $souceNode.SetAttribute("id", $souceNode.GetAttribute("id") + "_" +  $file.BaseName);
                    }
                    #select all the accessor, channel and input nodes to change the source
                    $accesorNodes = $aninNode.SelectNodes(".//ns:accessor | .//ns:channel | .//ns:input", $ns1);
                    foreach($accNode in $accesorNodes)
                    {
                        $accNode.SetAttribute("source", $accNode.GetAttribute("source") + "_" +  $file.BaseName);
                    }
                    $intanceAnimation = $xml.CreateElement("instance_animation");
                    $intanceAnimation.SetAttribute("url", "#" + $aninNode.GetAttribute("id"));
                    $newClip.AppendChild($intanceAnimation) | Out-Null;
                }
                $newClip.SetAttribute("start", $clipStartTime);
                $newClip.SetAttribute("end", $clipEndTime);
                $animClipsNode.AppendChild($newClip) | Out-Null;
                $animationNode.ParentNode.InsertAfter($xml.ImportNode($newNode,$true), $animationNode) | Out-Null;
            }
        }
    }
    Write-Progress -Activity "Processing animations" -Status "Done"

    $animationNode.ParentNode.InsertAfter($animClipsNode, $animationNode) | Out-Null;
    $morphArray = @()
    #Validate the morphs
    $controllersNode = $xml.SelectSingleNode("//ns:library_controllers", $ns)
    foreach($controller in $controllersNode.ChildNodes)
    {
        $skinNode = $controller.SelectSingleNode("ns:skin",$ns);
        if($skinNode)
        {
            $morphName =  $skinNode.GetAttribute("source").Substring(1) + "-morph";
            $morphController = $controllersNode.SelectSingleNode("ns:controller[@id='" + $morphName + "']",$ns); 
            if($morphController)
            {
                $morphController.FirstChild.SetAttribute("method", "NORMALIZED");
                $skinNode.SetAttribute("source", "#" + $morphName);
                $morphArray = $morphArray + $morphController.SelectSingleNode("ns:morph/ns:source/ns:IDREF_array",$ns).InnerText.Split(" ");
            }
        }
    }
    Write-Progress -Activity "Validate Morphs" -Status "Done"

    #Recalculate normals in morphs
    $geometriesNode = $xml.SelectSingleNode("//ns:library_geometries", $ns)
    $percentMorphs = 0;
    foreach($morphMesh in $morphArray)
    {
        $geometry = $geometriesNode.SelectSingleNode("ns:geometry[@id='" + $morphMesh + "']",$ns); 
        $polylist = $geometry.SelectSingleNode("ns:mesh/ns:polylist",$ns);
        if($null -ne $polylist)
        {
            $normalsSource = $polylist.SelectSingleNode("ns:input[@semantic='NORMAL']",$ns);
            $vtxLib = $geometry.SelectSingleNode("ns:mesh/ns:vertices/ns:input",$ns).GetAttribute("source").Replace("#","");
            $verticesArray = $geometry.SelectSingleNode("ns:mesh/ns:source[@id='"+ $vtxLib +"']/ns:float_array",$ns).InnerText.Trim() -split '\s+|\r\n';

            $normals = $geometry.SelectSingleNode("ns:mesh/ns:source[@id='"+ $normalsSource.GetAttribute("source").Replace("#","") +"']/ns:float_array",$ns);

            $vcountArray = $polylist.SelectSingleNode("ns:vcount",$ns).InnerText.Trim() -split '\s+|\r\n';
            $pArray = $polylist.SelectSingleNode("ns:p",$ns).InnerText.Trim() -split '\s+|\r\n';
            $newpArray = [int[]]::new($vcountArray.Count * 3);;
            
            $newpNormalsArray = [float[]]::new($verticesArray.Count);
            $finalpNormalsArray = [float[]]::new($vcountArray.Count *3 * 3);

            $poffset = 0;
            for($vxs = 0; $vxs-lt $vcountArray.Count; $vxs++)
            {
                $vidx1 = $pArray[($poffset * 3) + 0] -as [int];
                $vidx2 = $pArray[($poffset * 3) + 3] -as [int];
                $vidx3 = $pArray[($poffset * 3) + 6] -as [int];

                #get two edges
                $vtx1x = $verticesArray[($vidx1 * 3) + 0];
                $vtx1y = $verticesArray[($vidx1 * 3) + 1];
                $vtx1z = $verticesArray[($vidx1 * 3) + 2];

                $vtx2x = $verticesArray[($vidx2 * 3) + 0];
                $vtx2y = $verticesArray[($vidx2 * 3) + 1];
                $vtx2z = $verticesArray[($vidx2 * 3) + 2];

                $vtx3x = $verticesArray[($vidx3 * 3) + 0];
                $vtx3y = $verticesArray[($vidx3 * 3) + 1];
                $vtx3z = $verticesArray[($vidx3 * 3) + 2];

                $ux = $vtx2x - $vtx1x;
                $uy = $vtx2y - $vtx1y;
                $uz = $vtx2z - $vtx1z;

                $vx = $vtx3x - $vtx1x;
                $vy = $vtx3y - $vtx1y;
                $vz = $vtx3z - $vtx1z;

                $nx = (($uy * $vz) - ($uz * $vy));
                $ny = (($uz * $vx) - ($ux * $vz));
                $nz = (($ux * $vy) - ($uy * $vx));

                $newpNormalsArray[($vidx1 * 3) + 0] = $newpNormalsArray[($vidx1 * 3) + 0] + $nx;
                $newpNormalsArray[($vidx1 * 3) + 1] = $newpNormalsArray[($vidx1 * 3) + 1] + $ny;
                $newpNormalsArray[($vidx1 * 3) + 2] = $newpNormalsArray[($vidx1 * 3) + 2] + $nz;

                $newpNormalsArray[($vidx2 * 3) + 0] = $newpNormalsArray[($vidx2 * 3) + 0] + $nx;
                $newpNormalsArray[($vidx2 * 3) + 1] = $newpNormalsArray[($vidx2 * 3) + 1] + $ny;
                $newpNormalsArray[($vidx2 * 3) + 2] = $newpNormalsArray[($vidx2 * 3) + 2] + $nz;

                $newpNormalsArray[($vidx3 * 3) + 0] = $newpNormalsArray[($vidx3 * 3) + 0] + $nx;
                $newpNormalsArray[($vidx3 * 3) + 1] = $newpNormalsArray[($vidx3 * 3) + 1] + $ny;
                $newpNormalsArray[($vidx3 * 3) + 2] = $newpNormalsArray[($vidx3 * 3) + 2] + $nz;

                #Add reference to the new normals
                $pArray[($poffset * 3) + 1] = $poffset + 0;
                $pArray[($poffset * 3) + 4] = $poffset + 1;
                $pArray[($poffset * 3) + 7] = $poffset + 2;

                $newpArray[($poffset ) + 0] = $vidx1;
                $newpArray[($poffset ) + 1] = $vidx2;
                $newpArray[($poffset ) + 2] = $vidx3;

                $poffset += 3; #Poly size, but for clarity it's always 3
            }

            #normalize normals
            for($z = 0; $z -lt $newpNormalsArray.Count ; $z+=3)
            {
                $p = [Math]::Pow($newpNormalsArray[$z + 0],2);
                $q = [Math]::Pow($newpNormalsArray[$z + 1],2);
                $r = [Math]::Pow($newpNormalsArray[$z + 2],2);
                $d = [Math]::Sqrt($p + $q + $r);
                if($d -gt 0)
                {
                    $newpNormalsArray[$z + 0] = $newpNormalsArray[$z + 0] / $d;
                    $newpNormalsArray[$z + 1] = $newpNormalsArray[$z + 1] / $d;
                    $newpNormalsArray[$z + 2] = $newpNormalsArray[$z + 2] / $d;
                }
            }


            #reassing normals
            $poffset = 0;
            for($vxs = 0; $vxs-lt $vcountArray.Count; $vxs++)
            {
                $vidx1 = $pArray[($poffset * 3) + 0] -as [int];
                $vidx2 = $pArray[($poffset * 3) + 3] -as [int];
                $vidx3 = $pArray[($poffset * 3) + 6] -as [int];

                $finalpNormalsArray[($poffset * 3) + 0] = $newpNormalsArray[($vidx1 * 3) + 0] + $nx;
                $finalpNormalsArray[($poffset * 3) + 1] = $newpNormalsArray[($vidx1 * 3) + 1] + $ny;
                $finalpNormalsArray[($poffset * 3) + 2] = $newpNormalsArray[($vidx1 * 3) + 2] + $nz;

                $finalpNormalsArray[($poffset * 3) + 3] = $newpNormalsArray[($vidx2 * 3) + 0] + $nx;
                $finalpNormalsArray[($poffset * 3) + 4] = $newpNormalsArray[($vidx2 * 3) + 1] + $ny;
                $finalpNormalsArray[($poffset * 3) + 5] = $newpNormalsArray[($vidx2 * 3) + 2] + $nz;

                $finalpNormalsArray[($poffset * 3) + 6] = $newpNormalsArray[($vidx3 * 3) + 0] + $nx;
                $finalpNormalsArray[($poffset * 3) + 7] = $newpNormalsArray[($vidx3 * 3) + 1] + $ny;
                $finalpNormalsArray[($poffset * 3) + 8] = $newpNormalsArray[($vidx3 * 3) + 2] + $nz;

                $poffset += 3;
            }


            $normals.InnerText = "$finalpNormalsArray";
            $normals.SetAttribute("count", $finalpNormalsArray.Count);

            $accesor = $geometry.SelectSingleNode("ns:mesh/ns:source[@id='"+ $normalsSource.GetAttribute("source").Replace("#","") +"']/ns:technique_common/ns:accessor",$ns);
            $accesor.SetAttribute("count", ($finalpNormalsArray.Count / 3));

            $polylist.SelectSingleNode("ns:p",$ns).InnerText = "$pArray";
        }
        $percentMorphs++;
        Write-Progress -Activity "Recalculate Normals" -PercentComplete (($percentMorphs/$morphArray.Count) * 100)
    }
    $unitNode = $xml.SelectSingleNode("//ns:asset/ns:unit", $ns)
    $unitNode.SetAttribute("meter", "1.000000");
    $xml.Save($outputFile)
}
else 
{
    Write-Output "Invalid file name";
}
