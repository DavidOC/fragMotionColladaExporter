import bpy
context = bpy.context
obj = context.object

namelist = [

# Mobius Final Fantasy
("Hips", "mixamorig:Hips"),
("Spine", "mixamorig:Spine"),
("Chest", "mixamorig:Spine1"),
("Upper Chest", "mixamorig:Spine2"),
("Neck", "mixamorig:Neck"),
("Head", "mixamorig:Head"),
("Head Top", "mixamorig:HeadTop_End"),
("Eye_L", "mixamorig:LeftEye"),
("Eye_R", "mixamorig:RightEye"),
("Left shoulder", "mixamorig:LeftShoulder"),
("Left arm", "mixamorig:LeftArm"),
("Left elbow", "mixamorig:LeftForeArm"),
("Left wrist", "mixamorig:LeftHand"),
("Thumb0_L", "mixamorig:LeftHandThumb1"),
("Thumb1_L", "mixamorig:LeftHandThumb2"),
("Thumb2_L", "mixamorig:LeftHandThumb3"),
("Thumb3_L", "mixamorig:LeftHandThumb4"),
("IndexFinger1_L", "mixamorig:LeftHandIndex1"),
("IndexFinger2_L", "mixamorig:LeftHandIndex2"),
("IndexFinger3_L", "mixamorig:LeftHandIndex3"),
("IndexFinger4_L", "mixamorig:LeftHandIndex4"),
("MiddleFinger1_L", "mixamorig:LeftHandMiddle1"),
("MiddleFinger2_L", "mixamorig:LeftHandMiddle2"),
("MiddleFinger3_L", "mixamorig:LeftHandMiddle3"),
("MiddleFinger4_L", "mixamorig:LeftHandMiddle4"),
("RingFinger1_L", "mixamorig:LeftHandRing1"),
("RingFinger2_L", "mixamorig:LeftHandRing2"),
("RingFinger3_L", "mixamorig:LeftHandRing3"),
("RingFinger4_L", "mixamorig:LeftHandRing4"),
("LittleFinger1_L", "mixamorig:LeftHandPinky1"),
("LittleFinger2_L", "mixamorig:LeftHandPinky2"),
("LittleFinger3_L", "mixamorig:LeftHandPinky3"),
("LittleFinger4_L", "mixamorig:LeftHandPinky4"),
("Right shoulder", "mixamorig:RightShoulder"),
("Right arm", "mixamorig:RightArm"),
("Right elbow", "mixamorig:RightForeArm"),
("Right wrist", "mixamorig:RightHand"),
("LittleFinger1_R", "mixamorig:RightHandPinky1"),
("LittleFinger2_R", "mixamorig:RightHandPinky2"),
("LittleFinger3_R", "mixamorig:RightHandPinky3"),
("LittleFinger4_R", "mixamorig:RightHandPinky4"),
("RingFinger1_R", "mixamorig:RightHandRing1"),
("RingFinger2_R", "mixamorig:RightHandRing2"),
("RingFinger3_R", "mixamorig:RightHandRing3"),
("RingFinger4_R", "mixamorig:RightHandRing4"),
("MiddleFinger1_R", "mixamorig:RightHandMiddle1"),
("MiddleFinger2_R", "mixamorig:RightHandMiddle2"),
("MiddleFinger3_R", "mixamorig:RightHandMiddle3"),
("MiddleFinger4_R", "mixamorig:RightHandMiddle4"),
("IndexFinger1_R", "mixamorig:RightHandIndex1"),
("IndexFinger2_R", "mixamorig:RightHandIndex2"),
("IndexFinger3_R", "mixamorig:RightHandIndex3"),
("IndexFinger4_R", "mixamorig:RightHandIndex4"),
("Thumb0_R", "mixamorig:RightHandThumb1"),
("Thumb1_R", "mixamorig:RightHandThumb2"),
("Thumb2_R", "mixamorig:RightHandThumb3"),
("Thumb3_R", "mixamorig:RightHandThumb4"),
("Left leg", "mixamorig:LeftUpLeg"),
("Left knee", "mixamorig:LeftLeg"),
("Left ankle", "mixamorig:LeftFoot"),
("Left toe", "mixamorig:LeftToeBase"),
("Left toe end", "mixamorig:LeftToe_End"),
("Right leg", "mixamorig:RightUpLeg"),
("Right knee", "mixamorig:RightLeg"),
("Right ankle", "mixamorig:RightFoot"),
("Right toe", "mixamorig:RightToeBase"),
("Right toe end", "mixamorig:RightToe_End")

]

for name, newname in namelist:
	# get the pose bone with name
	pb = obj.pose.bones.get(name)
	# continue if no bone of that name
	if pb is None:
		continue
	# rename
	pb.name = newname